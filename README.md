SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

T. Kurz<br>
Investigation of different coupling schemes for hybrid-dimensional models <br>
Master's Thesis, 2018<br>
Universität Stuttgart

Installation
============

The easiest way to install this module is to create a new directory and clone this module:
```
mkdir Kurz2018a && cd Kurz2018a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Kurz2018a.git
```

After that, execute the file [installKurz2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Kurz2018a/raw/master/installKurz2018a.sh).
```
cd Kurz2018a
./installKurz2018a.sh
```

This should automatically download all necessary modules and check out the correct versions. Furthermore, a patch to `dumux` is applied and dunecontrol is run.

Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installKurz2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Kurz2018a/raw/master/installKurz2018a.sh).
