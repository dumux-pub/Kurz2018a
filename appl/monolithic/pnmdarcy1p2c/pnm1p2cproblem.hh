// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase pore network model.
 */
#ifndef DUMUX_PNM1P2C_PROBLEM_HH
#define DUMUX_PNM1P2C_PROBLEM_HH


// base problem
#include <dumux/porousmediumflow/problem.hh>
// Pore network model
#include <dumux/porenetworkflow/1pnc/model.hh>

// #include <dumux/material/fluidmatrixinteractions/spatialparams/porenetwork/porenetwork1p.hh>

#include <dumux/material/fluidsystems/h2on2.hh>

namespace Dumux
{
template <class TypeTag>
class PNMOnePTwoCProblem;

template<class TypeTag>
class CouplingFicksLaw;

// template<class TypeTag>
// class CouplingTransmissibilityAzzamDullien;

template<class Scalar, class TransmissibilityType>
class CouplingWashburnAdvection;

namespace Properties
{
#if ISOTHERMAL
NEW_TYPE_TAG(PNMOnePTwoCProblem, INHERITS_FROM(PNMOnePNC, PNMSpatialParams));
#endif

#if !ISOTHERMAL
NEW_TYPE_TAG(PNMOnePTwoCProblem, INHERITS_FROM(PNMOnePNCNI, PNMSpatialParams));
#endif

// Set the problem property
SET_TYPE_PROP(PNMOnePTwoCProblem, Problem, Dumux::PNMOnePTwoCProblem<TypeTag>);

// Set fluid configuration
SET_TYPE_PROP(PNMOnePTwoCProblem,
              FluidSystem,
              FluidSystems::H2ON2<typename GET_PROP_TYPE(TypeTag, Scalar), false>);

// Set the grid type
SET_TYPE_PROP(PNMOnePTwoCProblem, Grid, Dune::FoamGrid<1, 3>);

SET_TYPE_PROP(PNMOnePTwoCProblem, SinglePhaseTransmissibility, TransmissibilityAzzamDullien<TypeTag>);
// SET_TYPE_PROP(PNMOnePTwoCProblem, SinglePhaseTransmissibility, CouplingTransmissibilityAzzamDullien<TypeTag>);

SET_PROP(PNMOnePTwoCProblem, AdvectionType)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using TransmissibilityType = ThroatTransmissibility<TypeTag>;
public:
    using type = Dumux::CouplingWashburnAdvection<Scalar, TransmissibilityType/*, DiscretizationMethod::box*/>;
};

SET_STRING_PROP(PNMOnePTwoCProblem, ModelParameterGroup, "PNM");

SET_TYPE_PROP(PNMOnePTwoCProblem, MolecularDiffusionType, Dumux::CouplingFicksLaw<TypeTag>);

SET_BOOL_PROP(PNMOnePTwoCProblem, UseMoles, true);

}

template <class TypeTag>
class PNMOnePTwoCProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);


    // copy some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);
    using Labels = typename GET_PROP_TYPE(TypeTag, Labels);
    enum {
        // grid and world dimension
        dim = GridView::dimension,
        dimworld = GridView::dimensionworld,

        // primary variable indices
        conti0EqIdx = Indices::conti0EqIdx,
        pressureIdx = Indices::pressureIdx,
        transportEqIdx = 1,

#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
#endif

    };

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;

    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);

public:
    PNMOnePTwoCProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                       std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry), eps_(1e-7), couplingManager_(couplingManager)
    {


        updateBoundaryFactor();

        FluidSystem::init();
    }


#if ISOTHERMAL
    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 20; } // 20°C
    // \}
#endif
     /*!
     * \name Boundary conditions
     */
    // \{
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolume &scv) const
    {
        BoundaryTypes bcTypes;
        if (couplingManager().isCoupledDof(CouplingManager::lowDimIdx, scv.dofIndex()) || this->spatialParams().isOutletPore(scv))
            bcTypes.setAllDirichlet();
        else // neuman for the remaining boundaries
            bcTypes.setAllNeumann();
        return bcTypes;

    }

        /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex (pore body) for which the condition is evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolume &scv) const
    {
        PrimaryVariables values(0.0);
        if(couplingManager().isCoupledDof(CouplingManager::lowDimIdx, scv.dofIndex())){
            values[pressureIdx] = couplingManager().couplingData().bulkPrivarAtPos(scv.dofPosition())[pressureIdx];//0.9e5;
            values[transportEqIdx] = couplingManager().couplingData().bulkPrivarAtPos(scv.dofPosition())[transportEqIdx];//0.9e5;
        }
        else{
             values[pressureIdx] = 99980.0;//1e5;//
             values[transportEqIdx] = 0.0;//1.0e-5;//
        }



#if !ISOTHERMAL
         if(this->spatialParams().isInletPore(scv))
            values[temperatureIdx] = 273.15 +25;
         else
            values[temperatureIdx] = 273.15 +20;
#endif
         return values;
    }

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        vertex
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * units of \f$ [ \textnormal{unit of conserved quantity} / s] \f$
     * \param vertex The vertex
     * \param volVars All volume variables for the pore
     *
     * Positive values mean that mass is created,
     * negative ones mean that it vanishes.
     */
    PrimaryVariables source(const Vertex& vertex, const VolumeVariables& volVars) const
    {
        return PrimaryVariables(0);
    }
    // \}

        /*!
    * \brief Compute / update the flux variables
    *
    * \param problem The problem
    */
   void updateBoundaryFactor()
   {
       const auto& fvGridGeometry = this->fvGridGeometry();
       factor_.resize(fvGridGeometry.gridView().size(0), 1.0);

       // Decrease the transmissibility (and thereby the fluxes) by a factor of
       // 0.5 for pores on the boundary and by  a factor of 0.25 for pores in corners.
       // This is important for the coupling with box bulk domains. Otherwise, bulk scvs at the
       // boundaries or at corners would receive too much mass flow.
       // Using this factor, a homogeneous pressure distribution (for symmetrical configurations)
       // can be realized.
       const Scalar eps = 1e-8;

       // get the y and z bounding "box"
       const auto yMin = fvGridGeometry.bBoxMin()[1];
       const auto yMax = fvGridGeometry.bBoxMax()[1];
       const auto zMin = fvGridGeometry.bBoxMin()[2];
       const auto zMax = fvGridGeometry.bBoxMax()[2];

       for(const auto& element : elements(fvGridGeometry.gridView()))
       {
           const auto eIdx = fvGridGeometry.elementMapper().index(element);


           // get the y and z position of the element
           const auto y = element.geometry().center()[1];
           const auto z = element.geometry().center()[2];

           // treat pores on the boundary
           if(y < yMin + eps || y > yMax - eps || z < zMin + eps || z > zMax - eps)
               factor_[eIdx] *= 0.5;

           // treat pores at the corners
           if((y < yMin + eps || y > yMax - eps) && (z < zMin + eps || z > zMax - eps))
               factor_[eIdx] *= 0.5;
       }
   }

       Scalar boundaryFactor(int eIdx) const
    {
//         std::cout << "factor" << factor_ << std::endl;
        return factor_[eIdx];
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Vertex& vertex) const
    {

        PrimaryVariables values(0.0);
            values[pressureIdx] = 1.0e5;
            values[transportEqIdx] = 0.0;//1.0e-5;


#if !ISOTHERMAL
            values[temperatureIdx] = 273.15 +20;
#endif
            return values;
    }


    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    Scalar eps_;

    std::shared_ptr<CouplingManager> couplingManager_;
    std::vector<Scalar> factor_;
};


template<class TypeTag>
class CouplingFicksLaw : public PNMFicksLaw<TypeTag>
{
    using ParentType = PNMFicksLaw<TypeTag>;
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using ElementFluxVariablesCache = typename GET_PROP_TYPE(TypeTag, ElementFluxVariablesCache);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;

public:

    static auto flux(const Problem& problem,
                     const Element& element,
                     const FVElementGeometry& fvGeometry,
                     const ElementVolumeVariables& elemVolVars,
                     const SubControlVolumeFace& scvf,
                     const int phaseIdx,
                     const ElementFluxVariablesCache& elemFluxVarsCache)
    {
        const auto eIdx = problem.fvGridGeometry().elementMapper().index(element);
        auto flux = ParentType::flux(problem, element, fvGeometry, elemVolVars, scvf, phaseIdx, elemFluxVarsCache);
        flux *= problem.boundaryFactor(eIdx);
        return flux;
    }
};


template<class Scalar, class TransmissibilityType>
class CouplingWashburnAdvection : public WashburnAdvection<Scalar, TransmissibilityType, DiscretizationMethod::box>
{
    using ParentType = WashburnAdvection<Scalar, TransmissibilityType, DiscretizationMethod::box>;

public:
    template<class Problem, class Element, class FVElementGeometry,
             class ElementVolumeVariables, class SubControlVolumeFace, class ElemFluxVarsCache>
    static Scalar flux(const Problem& problem,
                       const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolumeFace& scvf,
                       const int phaseIdx,
                       const ElemFluxVarsCache& elemFluxVarsCache)
    {
        const auto eIdx = problem.fvGridGeometry().elementMapper().index(element);
        return ParentType::flux(problem, element, fvGeometry, elemVolVars, scvf, phaseIdx, elemFluxVarsCache)* problem.boundaryFactor(eIdx);
    }
};

} //end namespace

#endif
