// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase pore network model.
 */
#ifndef DUMUX_PNM1P_PROBLEM_HH
#define DUMUX_PNM1P_PROBLEM_HH


// base problem
#include <dumux/porousmediumflow/problem.hh>
// Pore network model
#include <dumux/porenetworkflow/1p/model.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>

#define ISOTHERMAL 1

namespace Dumux
{
template <class TypeTag>
class PNMOnePProblem;

template<class TypeTag>
class CouplingTransmissibilityAzzamDullien;

namespace Properties
{
#if ISOTHERMAL
NEW_TYPE_TAG(PNMOnePTypeTag, INHERITS_FROM(PNMOneP, PNMSpatialParams));
#endif

#if !ISOTHERMAL
NEW_TYPE_TAG(PNMOnePTypeTag, INHERITS_FROM(PNMOnePNI, PNMSpatialParams));
#endif

// Set the problem property
SET_TYPE_PROP(PNMOnePTypeTag, Problem, Dumux::PNMOnePProblem<TypeTag>);

// the fluid system
SET_PROP(PNMOnePTypeTag, FluidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using type = FluidSystems::LiquidPhase<Scalar, Dumux::Components::SimpleH2O<Scalar> > ;
};

// Set the grid type
SET_TYPE_PROP(PNMOnePTypeTag, Grid, Dune::FoamGrid<1, 3>);

SET_STRING_PROP(PNMOnePTypeTag, ModelParameterGroup, "PNM");

SET_TYPE_PROP(PNMOnePTypeTag, SinglePhaseTransmissibility, CouplingTransmissibilityAzzamDullien<TypeTag>);

SET_TYPE_PROP(PNMOnePTypeTag, PointSource, IdPointSource<TypeTag, std::size_t> );

}

template <class TypeTag>
class PNMOnePProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);

    // copy some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);
    using Labels = typename GET_PROP_TYPE(TypeTag, Labels);
    enum {
        // grid and world dimension
        dim = GridView::dimension,
        dimworld = GridView::dimensionworld,

        // primary variable indices
        conti0EqIdx = Indices::conti0EqIdx,
        pressureIdx = Indices::pressureIdx,

#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
#endif

    };

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;

    enum { dofCodim =  dim };

    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);

public:
    PNMOnePProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                   std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry), eps_(1e-7), couplingManager_(couplingManager)
    {
        updateBoundaryFactor();
    }

    /*!
     * \name Simulation steering
     */
    // \{

    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     */
    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteOutput() const //define output
    {
        return true;
    }



#if ISOTHERMAL
    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C
    // \}
#endif
     /*!
     * \name Boundary conditions
     */
    // \{
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolume &scv) const
    {
        BoundaryTypes bcTypes;
        if (couplingManager().isCoupledDof(CouplingManager::lowDimIdx, scv.dofIndex()) || this->spatialParams().isOutletPore(scv))
            bcTypes.setAllDirichlet();
        else // neuman for the remaining boundaries
            bcTypes.setAllNeumann();
        return bcTypes;
    }

        /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex (pore body) for which the condition is evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolume &scv) const
    {
        PrimaryVariables values(0.0);
        if(couplingManager().isCoupledDof(CouplingManager::lowDimIdx, scv.dofIndex()))
            values[pressureIdx] = couplingManager().couplingData().bulkPrivarAtPos(scv.dofPosition());//0.9e5;
        else
            values[pressureIdx] = 99980.0;
         return values;
    }


    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        vertex
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * units of \f$ [ \textnormal{unit of conserved quantity} / s] \f$
     * \param vertex The vertex
     * \param volVars All volume variables for the pore
     *
     * Positive values mean that mass is created,
     * negative ones mean that it vanishes.
     */
    PrimaryVariables source(const Vertex& vertex, const VolumeVariables& volVars) const
    {
        return PrimaryVariables(0);
    }

    /*!
    * \brief Compute / update the flux variables
    *
    * \param problem The problem
    */
   void updateBoundaryFactor()
   {
       const auto& fvGridGeometry = this->fvGridGeometry();
       factor_.resize(fvGridGeometry.gridView().size(0), 1.0);

       // Decrease the transmissibility (and thereby the fluxes) by a factor of
       // 0.5 for pores on the boundary and by  a factor of 0.25 for pores in corners.
       // This is important for the coupling with box bulk domains. Otherwise, bulk scvs at the
       // boundaries or at corners would receive too much mass flow.
       // Using this factor, a homogeneous pressure distribution (for symmetrical configurations)
       // can be realized.
       const Scalar eps = 1e-8;

       // get the y and z bounding "box"
       const auto yMin = fvGridGeometry.bBoxMin()[1];
       const auto yMax = fvGridGeometry.bBoxMax()[1];
       const auto zMin = fvGridGeometry.bBoxMin()[2];
       const auto zMax = fvGridGeometry.bBoxMax()[2];

       for(const auto& element : elements(fvGridGeometry.gridView()))
       {
           const auto eIdx = fvGridGeometry.elementMapper().index(element);


           // get the y and z position of the element
           const auto y = element.geometry().center()[1];
           const auto z = element.geometry().center()[2];

           // treat pores on the boundary
           if(y < yMin + eps || y > yMax - eps || z < zMin + eps || z > zMax - eps)
               factor_[eIdx] *= 0.5;

           // treat pores at the corners
           if((y < yMin + eps || y > yMax - eps) && (z < zMin + eps || z > zMax - eps))
               factor_[eIdx] *= 0.5;
       }
   }

    // \}

    Scalar boundaryFactor(int eIdx) const
    {
        return factor_[eIdx];
    }

    // \}

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    Scalar eps_;
    std::shared_ptr<CouplingManager> couplingManager_;
    std::vector<Scalar> factor_;



};

template<class TypeTag>
class CouplingTransmissibilityAzzamDullien : public TransmissibilityAzzamDullien<TypeTag>
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using ParentType = TransmissibilityAzzamDullien<TypeTag>;

public:

    /*!
    * \brief Returns the conductivity of a prismatic throat with square-shaped cross-section
    *
    * \param problem The problem
    * \param element The element
    * \param ElementVolumeVariables The element volume variables
    */
   static Scalar singlePhaseTransmissibility(const Problem &problem,
                                             const Element &element,
                                             const ElementVolumeVariables &elemVolVars)
   {
       const auto eIdx = problem.fvGridGeometry().elementMapper().index(element);
       return ParentType::singlePhaseTransmissibility(problem, element, elemVolVars) * problem.boundaryFactor(eIdx);
   }

};

} //end namespace

#endif
