// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the 1d-3d embedded mixed-dimension model coupling two
 *        one-phase porous medium flow problems
 */
#include <config.h>

#include <ctime>
#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/geometry/diameter.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/methods.hh>
#include <dumux/io/vtkoutputmodule.hh>

#include <dumux/multidomain/traits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include <dumux/mixeddimension/embedded/integrationpointsource.hh>
#include <dumux/porenetworkflow/common/pnmvtkoutputmodule.hh>

#include <dumux/mixeddimension/boundary/pnmdarcy/couplingmanager.hh>

#include "pnm1pproblem.hh"
#include "darcy1pproblem.hh"

namespace Dumux {
namespace Properties {

SET_PROP(DarcyOnePTestTypeTag, CouplingManager)
{
    using Traits = MultiDomainTraits<TypeTag, TTAG(PNMOnePTypeTag)>;
    using type = Dumux::PNMDarcyCouplingManager<Traits>;
};

SET_PROP(PNMOnePTypeTag, CouplingManager)
{
    using Traits = MultiDomainTraits<TTAG(DarcyOnePTestTypeTag), TypeTag>;
    using type = Dumux::PNMDarcyCouplingManager<Traits>;
};

// TODO
// SET_TYPE_PROP(DarcyOnePTestTypeTag, PointSource, IntegrationPointSource<TypeTag>);


} // end namespace Properties
} // end namespace Dumux

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using BulkTypeTag = TTAG(DarcyOnePTestTypeTag);
    using LowDimTypeTag = TTAG(PNMOnePTypeTag);

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    using BulkGridCreator = typename GET_PROP_TYPE(BulkTypeTag, GridCreator);
    BulkGridCreator::makeGrid("Darcy"); // pass parameter group

    using LowDimGridCreator = typename GET_PROP_TYPE(LowDimTypeTag, GridCreator);
    LowDimGridCreator::makeGrid("PNM"); // pass parameter group

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& bulkGridView = BulkGridCreator::grid().leafGridView();
    const auto& lowDimGridView = LowDimGridCreator::grid().leafGridView();

    // create the finite volume grid geometry
    using BulkFVGridGeometry = typename GET_PROP_TYPE(BulkTypeTag, FVGridGeometry);
    auto bulkFvGridGeometry = std::make_shared<BulkFVGridGeometry>(bulkGridView);
    bulkFvGridGeometry->update();
    using LowDimFVGridGeometry = typename GET_PROP_TYPE(LowDimTypeTag, FVGridGeometry);
    auto lowDimFvGridGeometry = std::make_shared<LowDimFVGridGeometry>(lowDimGridView);
    lowDimFvGridGeometry->update();

    // the mixed dimension type traits
    using Traits = MultiDomainTraits<BulkTypeTag, LowDimTypeTag>;
    constexpr auto bulkIdx = Traits::template DomainIdx<0>();
    constexpr auto lowDimIdx = Traits::template DomainIdx<1>();

    // // the coupling manager
    using CouplingManager = PNMDarcyCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>(bulkFvGridGeometry, lowDimFvGridGeometry);
    //
    // // the problem (initial and boundary conditions)
    using BulkProblem = typename GET_PROP_TYPE(BulkTypeTag, Problem);
    auto bulkProblem = std::make_shared<BulkProblem>(bulkFvGridGeometry, couplingManager);
    using LowDimProblem = typename GET_PROP_TYPE(LowDimTypeTag, Problem);
    auto lowDimProblem = std::make_shared<LowDimProblem>(lowDimFvGridGeometry, couplingManager);

    // the solution vector
    Traits::SolutionVector sol;
    sol[bulkIdx].resize(bulkFvGridGeometry->numDofs());
    sol[lowDimIdx].resize(lowDimFvGridGeometry->numDofs());

    couplingManager->init(bulkProblem, lowDimProblem, sol);
    bulkProblem->computePointSourceMap();

    // the grid variables
    using BulkGridVariables = typename GET_PROP_TYPE(BulkTypeTag, GridVariables);
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkFvGridGeometry);
    bulkGridVariables->init(sol[bulkIdx]);
    using LowDimGridVariables = typename GET_PROP_TYPE(LowDimTypeTag, GridVariables);
    auto lowDimGridVariables = std::make_shared<LowDimGridVariables>(lowDimProblem, lowDimFvGridGeometry);
    lowDimGridVariables->init(sol[lowDimIdx]);

    // intialize the vtk output modules
    const auto bulkName = getParam<std::string>("Problem.Name") + "_" + bulkProblem->name();
    const auto lowDimName = getParam<std::string>("Problem.Name") + "_" + lowDimProblem->name();

    VtkOutputModule<BulkTypeTag> bulkVtkWriter(*bulkProblem, *bulkFvGridGeometry, *bulkGridVariables, sol[bulkIdx], bulkName);
    GET_PROP_TYPE(BulkTypeTag, VtkOutputFields)::init(bulkVtkWriter);
    bulkVtkWriter.write(0.0);

    PNMVtkOutputModule<LowDimTypeTag> lowDimVtkWriter(*lowDimProblem, *lowDimFvGridGeometry, *lowDimGridVariables, sol[lowDimIdx], lowDimName);
    GET_PROP_TYPE(LowDimTypeTag, VtkOutputFields)::init(lowDimVtkWriter);
    lowDimVtkWriter.write(0.0);


    // the assembler with time loop for instationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(bulkProblem, lowDimProblem),
                                                 std::make_tuple(bulkFvGridGeometry, lowDimFvGridGeometry),
                                                 std::make_tuple(bulkGridVariables, lowDimGridVariables),
                                                 couplingManager);

    // the linear solver
//     using LinearSolver = ILUnBiCGSTABBackend;
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

//     // assemble and solve the problem monolithically
//     // the non-linear solver
//     using MonolithicNewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
//     MonolithicNewtonSolver monolithicNonLinearSolver(assembler, linearSolver, couplingManager);
//     monolithicNonLinearSolver.solve(sol);
//
//     bulkVtkWriter.write(1.0);
//     lowDimVtkWriter.write(1.0);
//
//     // assemble and solve the problem sequentially
//     // reset the solution and related quantities
//     sol[bulkIdx] = 0.0;
//     sol[lowDimIdx] = 0.0;
//     bulkProblem->applyInitialSolution(sol[bulkIdx]);
//     lowDimProblem->applyInitialSolution(sol[lowDimIdx]);
//     Traits::SolutionVector solOld = sol;
//     bulkGridVariables->init(sol[bulkIdx], solOld[bulkIdx]);
//     lowDimGridVariables->init(sol[lowDimIdx], solOld[lowDimIdx]);
//     couplingManager->updateSolution(sol);

    auto bulkAssembler = makeSequentialAssembler(assembler, bulkIdx);
    auto lowDimAssembler = makeSequentialAssembler(assembler, lowDimIdx);

    using BulkNewtonSolver = NewtonSolver<typename decltype(bulkAssembler)::element_type, LinearSolver>;
    BulkNewtonSolver bulkNewtonSolver(bulkAssembler, linearSolver);

    using LowDimNewtonSolver = NewtonSolver<typename decltype(lowDimAssembler)::element_type, LinearSolver>;
    LowDimNewtonSolver lowDimNewtonSolver(lowDimAssembler, linearSolver);

    using Scalar = typename Traits::Scalar;
    static const int maxIterations = getParam<int>("MultiDomain.MaxIterations", 10);
    static const Scalar tolerance = getParam<Scalar>("MultiDomain.Tolerance", 1e-6);

    static const Scalar dampingFactor = getParam<Scalar>("MultiDomain.DampingFactor", 0.5);
    if(dampingFactor < 0.0 || dampingFactor > 1.0)
        DUNE_THROW(Dumux::ParameterException, "DampingFactor must be between 0 and 1");

    auto prevSolBulk = sol[bulkIdx];
    auto prevSolLowDim = sol[lowDimIdx];

    std::cout << "Solving sequentially with a tolerance of " << tolerance << std::endl;

    std::ofstream file;
    file.open("coupling_output.txt");
    file << "#iterations,  bulkAssembleTime, bulkSolveTime, bulkUpdateTime, bulkNewtonSteps, lowDimAssembleTime, lowDimSolveTime, lowDimUpdateTime, lowDimNewtonSteps, totalSolveTime" << std::endl;

    std::ofstream file1;
    file1.open("newton_output.txt");
    file1 << "# sequentialIteration, bulkNewtonSteps, lowDimNewtonSteps, error" << std::endl;

    //solve iterative
    auto solve = [&](){
        Scalar error = std::numeric_limits<Scalar>::max();
        int iterations = 0;
        int bulkNewtonSteps = 0;
        int lowDimNewtonSteps = 0;
        Scalar bulkAssembleTime = 0.0;
        Scalar bulkSolveTime = 0.0;
        Scalar bulkUpdateTime = 0.0;
        Scalar lowDimAssembleTime = 0.0;
        Scalar lowDimSolveTime = 0.0;
        Scalar lowDimUpdateTime = 0.0;

        while(error > tolerance)
        {
            if(iterations >= maxIterations)
                DUNE_THROW(Dumux::NumericalProblem, "Iterative algorithm didn't converge in " + std::to_string(maxIterations) + " iterations!");

            // solve the bulk problem first
            std::cout << "Solving bulk problem\n";
            bulkNewtonSolver.solve(sol[bulkIdx]);
            bulkNewtonSteps += bulkNewtonSolver.newtonSteps();
            file1 << " " << iterations << " " << bulkNewtonSolver.newtonSteps() << " ";

            // update the solution for the coupling manager using a damping factor to avoid oscillations
            auto tmp = sol[bulkIdx];
            for(int  i = 0; i < sol[bulkIdx].size(); ++i)
                tmp[i] = (1.0 - dampingFactor)*sol[bulkIdx][i] + dampingFactor*couplingManager->curSol()[bulkIdx][i];
            couplingManager->updateSolution(tmp, bulkIdx);

            // solve the low dim problem
            std::cout << "Solving low dim problem\n";
            lowDimNewtonSolver.solve(sol[lowDimIdx]);
            lowDimNewtonSteps += lowDimNewtonSolver.newtonSteps();
            file1 << lowDimNewtonSolver.newtonSteps() << " ";
            couplingManager->updateSolution(sol[lowDimIdx], lowDimIdx);

            // calculate discrete l2-norm of pressure
            auto solBulkDiff = prevSolBulk;
            solBulkDiff -= sol[bulkIdx];
            auto solLowDimDiff = prevSolLowDim;
            solLowDimDiff -= sol[lowDimIdx];

            // make the new solution the old solution
            prevSolBulk = sol[bulkIdx];
            prevSolLowDim = sol[lowDimIdx];

            const auto pBulkNorm = solBulkDiff.infinity_norm()/sol[bulkIdx].infinity_norm();
            const auto pLowDimNorm = solLowDimDiff.infinity_norm()/sol[lowDimIdx].infinity_norm();

            //time
            bulkAssembleTime += bulkNewtonSolver.assembleTime();
            bulkSolveTime +=  bulkNewtonSolver.solveTime();
            bulkUpdateTime += bulkNewtonSolver.updateTime();
            lowDimAssembleTime += lowDimNewtonSolver.assembleTime();
            lowDimSolveTime += lowDimNewtonSolver.solveTime();
            lowDimUpdateTime += lowDimNewtonSolver.updateTime();


            // calculate the error
            error = pBulkNorm + pLowDimNorm;
            file1 << error << std::endl;
            std::cout << "** Sequential algorithm: iteration " << iterations << ", error " << error << " **" << std::endl;
            ++iterations;
/*
            bulkVtkWriter.write(iterations);
            lowDimVtkWriter.write(iterations);*/
        }

        file  << iterations << " " << bulkAssembleTime << " " << bulkSolveTime << " " << bulkUpdateTime << " " << bulkNewtonSteps << " " << lowDimAssembleTime << " " << lowDimSolveTime << " " << lowDimUpdateTime << " " << lowDimNewtonSteps << " ";

    };



        // try solving the non-linear system
        Dune::Timer solveTimer;
        solve();
        solveTimer.stop();

        file << solveTimer.elapsed() << std::endl;
        file.close();
        file1.close();

        // write vtk output
        bulkVtkWriter.write(1.0);
        lowDimVtkWriter.write(1.0);

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
