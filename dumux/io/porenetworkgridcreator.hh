// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Provides a grid creator builds a pore network grid from a host grid
 */
#ifndef DUMUX_PORE_GRID_CREATOR_HH
#define DUMUX_PORE_GRID_CREATOR_HH

#include <memory>
#include <random>

#include <dune/common/exceptions.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/utility/structuredgridfactory.hh>


#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif
#include <dune/grid/yaspgrid.hh>

#include <dune/grid/common/mcmgmapper.hh>
#include <dune/grid/utility/persistentcontainer.hh>
#include <dune/grid/io/file/dgfparser/dgfparser.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/porenetworkflow/common/geometry.hh>
#include <dumux/porenetworkflow/common/labels.hh>

namespace Dumux
{

namespace Properties
{
// forward declarations
NEW_PROP_TAG(Scalar);
NEW_PROP_TAG(Grid);
NEW_PROP_TAG(GridParameterGroup);
NEW_PROP_TAG(PoreGeometry);
}

// forward declaration
template<class TypeTag>
class PoreNetworkGridCreator;

/*!
 * \brief Auxiliary class which simplifies handling of input paramters
 */
template<class TypeTag>
class PNMGridCreatorInputHelper
{
    using Grid = typename GET_PROP_TYPE(TypeTag, Grid);
    using GridView = typename Grid::LeafGridView;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    enum {
        dim = GridView::dimension,
        dimworld = GridView::dimensionworld
    };

    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;
    using BoundaryFaceMarker = std::array<int, 2*dimworld>;
    using PriorityList = std::array<int, 2*dimworld>;

    using PersistentParameterContainer =  Dune::PersistentContainer<Grid, std::vector<Scalar>>;
public:

    /*!
     * \brief Returns whether a grid-related parameter is given in the input file
     *
     * \param parameterName The name of the paramter
     */
    static bool parameterGiven(const std::string& parameterName)
    {
        return haveParamInGroup(GET_PROP_VALUE(TypeTag, ModelParameterGroup), "Grid." + parameterName);
    }

    /*!
     * \brief Returns a list of boundary face indices from user specified input or default values if no input is given
     */
    static auto getBoundaryFacemarker()
    {
        BoundaryFaceMarker boundaryFaceMarker;
        std::fill(boundaryFaceMarker.begin(), boundaryFaceMarker.end(), 0);
        boundaryFaceMarker[0] = 1;
        boundaryFaceMarker[1] = 1;

        if(parameterGiven("BoundaryFaceMarker"))
        {
            try {
                boundaryFaceMarker = getParamFromGroup<BoundaryFaceMarker>(GET_PROP_VALUE(TypeTag, ModelParameterGroup), "Grid.BoundaryFaceMarker");
            }
            catch (Dune::RangeError& e) {
                DUNE_THROW(Dumux::ParameterException, "You must specifiy all boundaries faces: xmin xmax ymin ymax (zmin zmax). \n" << e.what());
            }
            if(std::none_of(boundaryFaceMarker.begin(), boundaryFaceMarker.end(), []( const int i ){ return i == 1; }))
                DUNE_THROW(Dumux::ParameterException, "At least one face must have index 1");
            if(std::any_of(boundaryFaceMarker.begin(), boundaryFaceMarker.end(), []( const int i ){ return (i < 0 || i > 2*dimworld); }))
                DUNE_THROW(Dumux::ParameterException, "Face indices must range from 0 to " << 2*dimworld );
        }
        return boundaryFaceMarker;
    }

    /*!
     * \brief Returns a list of boundary face priorities from user specified input or default values if no input is given
     *
     * This essentially determines the index of a node on an edge or corner corner. For instance, a list of {0,1,2} will give highest priority
     * to the "x"-faces and lowest to the "z-faces".
     */
    static auto getPriorityList()
    {
        const static auto list = []()
        {
            PriorityList priorityList;
            std::iota(priorityList.begin(), priorityList.end(), 0);

            if(parameterGiven("PriorityList"))
            {
                try {
                    // priorities can also be set in the input file
                    priorityList = getParamFromGroup<PriorityList>(GET_PROP_VALUE(TypeTag, ModelParameterGroup), "Grid.PriorityList");
                }
                // make sure that a priority for each direction is set
                catch(Dune::RangeError& e) {
                    DUNE_THROW(Dumux::ParameterException, "You must specifiy priorities for all directions (" << dimworld << ") \n" << e.what());
                }
                // make sure each direction is only set once
                auto isUnique = [] (auto v)
                {
                    std::sort(v.begin(), v.end());
                    return (std::unique(v.begin(), v.end()) == v.end());
                };
                if(!isUnique(priorityList))
                    DUNE_THROW(Dumux::ParameterException, "You must specifiy priorities for all directions (duplicate directions)");

                //make sure that the directions are correct (ranging from 0 to dimworld-1)
                if(std::any_of(priorityList.begin(), priorityList.end(), []( const int i ){ return (i < 0 || i >= 2*dimworld); }))
                    DUNE_THROW(Dumux::ParameterException, "You must specifiy priorities for correct directions (0-" << 2*(dimworld-1) << ")");
            }
            return priorityList;
        }();
        return list;
    }

    /*!
     * \brief Returns a list of labels which indicates which throats to remove on the boundary
     */
    static auto removeThroatsOnBoundary()
    {
        auto result = getParamFromGroup<std::vector<int>>(GET_PROP_VALUE(TypeTag, ModelParameterGroup), "Grid.RemoveThroatsOnBoundary", std::vector<int>());
        if(std::any_of(result.begin(), result.end(), [](const int i){ return i >= 2*dimworld; }))
            DUNE_THROW(Dumux::ParameterException, "You can only delete throats on " << 2*(dimworld) << " sides.");

        return result;
    }

    /*!
     * \brief Returns whether to write out the host grid as a vtk file or not
     */
    static bool outputHostGrid()
    {
        return getParamFromGroup<bool>(GET_PROP_VALUE(TypeTag, ModelParameterGroup), "Grid.OutputHostGrid", false);
    }
};

/*!
 * \brief Auxiliary class that provides methods to assign data to the network grid
 */
template <class TypeTag>
class PNMGridCreatorParameterHelper
{
    using Grid = typename GET_PROP_TYPE(TypeTag, Grid);
    using GridView = typename Grid::LeafGridView;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Element = typename GridView::template Codim<0>::Entity;

    enum {
        dim = GridView::dimension,
        dimworld = GridView::dimensionworld
    };

    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;
    using BoundaryFaceMarker = std::array<int, 2*dimworld>;
    using PriorityList = std::array<int, dimworld>;

    using PersistentParameterContainer =  Dune::PersistentContainer<Grid, std::vector<Scalar>>;

    using Input = Dumux::PNMGridCreatorInputHelper<TypeTag>;
    using GridCreator = typename GET_PROP_TYPE(TypeTag, GridCreator);

public:

    /*!
     * \brief Returns the boundary face marker index at given position
     *
     * \param pos The current position
     * \param bboxMin The most lower left point of the domain
     * \param bboxMax The most upper right point of the domain
     */
    static int boundaryFaceMarkerAtPos(const GlobalPosition &pos,
                                       const GlobalPosition &bboxMin,
                                       const GlobalPosition &bboxMax)
    {
        static const auto boundaryFaceMarker = Input::getBoundaryFacemarker();
        const auto priorityList = Input::getPriorityList();
        constexpr Scalar eps = 1e-6;
        // set the priority which decides the order the vertices on the boundary are indexed
        // by default, vertices on min/max faces in x direcetion have the highest priority, followed by y and z
        for (auto i : priorityList)
        {
            const int idx = [i] ()
            {
                if(i < 2)
                    return 0;
                else if(i < 4)
                    return 1;
                else
                    return 2;
            } ();

            auto isOdd = [] (int n) { return (n % 2 != 0); };
            if(isOdd(i))
            {
                if(pos[idx] > bboxMax[idx] - eps)
                   return boundaryFaceMarker[i];
            }
            else
            {
                if(pos[idx] < bboxMin[idx] + eps)
                    return boundaryFaceMarker[i];
            }
        }
        return -1;
    }

    /*!
     * \brief Computes and returns the label of a given throat
     *
     * \param element The element (throat)
     */
    static auto throatLabel(const Element &element)
    {
        const auto boundaryIdx = Dumux::PoreNetworkGridCreator<TypeTag>::boundaryIdx;
        const auto vertex1 = element.template subEntity<dim>(0);
        const auto vertex2 = element.template subEntity<dim>(1);
        const int marker1 = GridCreator::parameters(vertex1)[boundaryIdx];
        const int marker2 = GridCreator::parameters(vertex2)[boundaryIdx];

        const auto priorityList = Input::getPriorityList();
        const auto boundaryFaceMarker = Input::getBoundaryFacemarker();

        if(marker1 == marker2) // both vertices are inside the domain or on the same boundary face
            return marker1;
        if(marker1 == -1) // vertex1 is inside the domain, vertex2 is on a boundary face
            return marker2;
        if(marker2 == -1) // vertex2 is inside the domain, vertex1 is on a boundary face
            return marker1;

        for(const auto i : priorityList) // vertex1 and vertex2 are on different boundary faces
        {
            if(marker1 == boundaryFaceMarker[i])
                return marker1;
            if(marker2 == boundaryFaceMarker[i])
                return marker2;
        }
        DUNE_THROW(Dune::InvalidStateException, "Something went wrong with the throat labels");
        return -99;
    }

    /*!
     * \brief Initializes and returns a container for vertex data
     *
     * \param grid The grid
     * \param numVertexParams The number of vertex paramters
     */
    static auto makeVertexParamContainer(const Grid& grid, const unsigned int numVertexParams)
    {
        auto vertexParameters = std::make_unique<PersistentParameterContainer>(grid, dim);
        (*vertexParameters).resize();
        for (auto&& v : (*vertexParameters))
            v.resize(numVertexParams);
        return std::move(vertexParameters);
    }

    /*!
     * \brief Initializes and returns a container for element data
     *
     * \param grid The grid
     * \param numElementParams The number of element paramters
     */
    static auto makeElementParamContainer(const Grid& grid, const unsigned int numElementParams)
    {
        auto elementParameters = std::make_unique<PersistentParameterContainer>(grid, 0);
        (*elementParameters).resize();
        for (auto&& v : (*elementParameters))
            v.resize(numElementParams);
        return std::move(elementParameters);
    }

    /*!
     * \brief Assign data for pore throat radius, pore throat length and pore body radius
     *
     * \param assignPoreRadius Assign pore radii or not
     * \param assignThroatRadius Assign throat radii or not
     */
    static void assignParameters()
    {
        static const auto modelParamGroup = GET_PROP_VALUE(TypeTag, ModelParameterGroup);
        // get input values related to parameters (throat radii and pore radii)
        bool constantParamaters = true;
        try
        {
            const auto type = getParamFromGroup<std::string>(modelParamGroup, "Grid.ParameterType");
            if (type == "const")
                constantParamaters = true;
            else if (type == "lognormal")
                constantParamaters = false;
            else
                DUNE_THROW(Dune::InvalidStateException, "Unknown parameter type " << type);

        }
        catch (Dumux::ParameterException &e) { }

        const Scalar minThroatLength = getParamFromGroup<Scalar>(modelParamGroup, "Grid.MinThroatLength", 1e-6);

        // if we use a distribution get the mean and standard deviation from input file
        Scalar meanPoreRadius(0), stddevPoreRadius(0);
        Scalar throatRadius(0), poreRadius(0);
        if (!constantParamaters)
        {
            meanPoreRadius = getParamFromGroup<Scalar>(modelParamGroup, "Grid.MeanPoreRadius");
            stddevPoreRadius = getParamFromGroup<Scalar>(modelParamGroup, "Grid.StandardDeviationPoreRadius");
        }
        else
            poreRadius = getParamFromGroup<Scalar>(modelParamGroup, "Grid.PoreRadius");

        // uniform random number generator for threshold checking
        std::random_device rd;
        std::mt19937 generator(rd());
        std::uniform_real_distribution<> uniformdist(0, 1);


        const auto& gridView = GridCreator::grid().leafGridView();

        // generate parameter sets (pore volume, throat diameter)
        // aliases
        auto& vertexParameters = *(GridCreator::vertexParameters_);
        auto& elementParameters = *(GridCreator::elementParameters_);

        // lognormal random number generator
        std::normal_distribution<> poreRadiusDist(meanPoreRadius, stddevPoreRadius);
        bool fixedThroatRadius = false;
        Scalar n = 0.1;
        try {
            throatRadius = getParamFromGroup<Scalar>(modelParamGroup, "Grid.ThroatRadius");
            fixedThroatRadius = true;
        }
        catch (Dumux::ParameterException &e) { }
        try { n = getParamFromGroup<Scalar>(modelParamGroup, "Grid.ThroatRadiusN"); }
        catch (Dumux::ParameterException &e) { }


        const int numVertices = gridView.size(dim);
        std::vector<int> poreRadiusLimited(numVertices, 0);
        std::vector<Scalar> maxPoreRadius(numVertices, 1e9);
        // get the maxium pore body radius to prevent pore body intersections
        // we define this as 1/2 of the length (minus a user specified value) of the shortest pore throat attached to the pore body
        for (const auto& element : elements(gridView))
        {
            const Scalar delta = element.geometry().volume();
            const Scalar maxRadius = (delta - minThroatLength)/2.0;
            for(int vIdxLocal = 0; vIdxLocal < 2 ; ++vIdxLocal)
            {
                const int vIdxGlobal = gridView.indexSet().subIndex(element, vIdxLocal, dim);
                maxPoreRadius[vIdxGlobal] = std::min(maxPoreRadius[vIdxGlobal], maxRadius);
            }
        }

        // set parameters
        for (const auto& element : elements(gridView))
        {
            const Scalar delta = element.geometry().volume();
            typedef typename GridView::template Codim<dim>::Entity PNMVertex;
            const auto vertex1 = element.template subEntity<dim>(0);
            const auto vertex2 = element.template subEntity<dim>(1);
            std::vector<PNMVertex> vertices = {vertex1, vertex2};

            for(int vIdxLocal = 0; vIdxLocal < 2 ; ++vIdxLocal)
            {
                const auto vIdxGlobal = gridView.indexSet().subIndex(element, vIdxLocal, dim);
                auto vertex = vertices[vIdxLocal];
                if(!constantParamaters)
                    poreRadius = poreRadiusDist(generator);
                if(poreRadius > maxPoreRadius[vIdxGlobal])
                    poreRadiusLimited[vIdxGlobal] = 1;
                vertexParameters[vertex][GridCreator::poreRadiusIdx] = std::min(poreRadius, maxPoreRadius[vIdxGlobal]);
            }

            // the element parameters (throat radius and length)

            if(fixedThroatRadius)
                elementParameters[element][GridCreator::throatRadiusIdx] = throatRadius;
            else
            {
                const Scalar poreRadius1 = GridCreator::parameters(vertex1)[GridCreator::poreRadiusIdx];
                const Scalar poreRadius2 = GridCreator::parameters(vertex2)[GridCreator::poreRadiusIdx];
                elementParameters[element][GridCreator::throatRadiusIdx] = averagedThroatRadius(poreRadius1, poreRadius2, delta, n) ;
            }

            // lambda to either use a user-specified value for the throat length or calculate it automatically based on the cell center to cell center
            // distance of the pore-bodies minus the two pore radii
            static const auto length = [&]
            {
                if(Input::parameterGiven("ThroatLength"))
                    return getParamFromGroup<Scalar>(modelParamGroup, "Grid.ThroatLength");

                const bool substractRadiiFromThroatLength = getParamFromGroup<bool>(modelParamGroup, "Grid.SubstractRadiiFromThroatLength");

                if(substractRadiiFromThroatLength)
                    return delta - vertexParameters[vertex1][0] - vertexParameters[vertex2][0];
                else
                    return delta;
            } ();

            if (std::signbit(length))
                DUNE_THROW(Dune::GridError, "Pore radii are so large they intersect! Something went wrong.");

            elementParameters[element][GridCreator::throatLengthIdx] = length;

            // set the throat label
            elementParameters[element][GridCreator::throatLabelIdx] = throatLabel(element);
        }
        const int numLimited = std::accumulate(poreRadiusLimited.begin(), poreRadiusLimited.end(), 0);
        if(numLimited)
            std::cout << "*******\nWarning!  " << numLimited << " out of " << numVertices
            << " pore body radii have been capped automatically in order to prevent intersecting pores\n*******" << std::endl;

    }

    /*!
     * \brief Computes and returns the coordination number for each pore
     */
    static auto computeCoordinationNumbers()
    {
        const auto gridView = GridCreator::grid().leafGridView();
        std::vector<unsigned int> coordNum(gridView.size(dim));
        for(const auto &element : elements(gridView))
        {
            for(int vIdxLocal = 0; vIdxLocal < 2; ++vIdxLocal)
            {
                const int vIdxGlobal = gridView.indexSet().subIndex(element, vIdxLocal, dim);
                coordNum[vIdxGlobal] +=1;
            }
        }
        assert(std::all_of(coordNum.begin(), coordNum.end(), [](int i){ return i > 0; })
               && "One of the pores is not connected to another pore. Check your grid file");
        return coordNum;
    }
};

/*!
 * \brief Auxiliary class which computes and stores some properties of the network grid
 */
template<class TypeTag>
class GridProperties
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Grid = typename GET_PROP_TYPE(TypeTag, Grid);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Parameters = Dune::PersistentContainer<Grid, std::vector<Scalar>>;
    using GridCreator = typename GET_PROP_TYPE(TypeTag, GridCreator);
    using GridType = typename Dumux::PoreNetworkGridCreator<TypeTag>::GridType;

    enum {
        dim = GridView::dimension,
        dimworld = GridView::dimensionworld
    };

public:

    GridProperties() {}

    /*!
     * \brief Updates the grid properties
     */
    void update()
    {
        gridType_ = GridCreator::type_;
        const auto& gridView = GridCreator::grid().leafGridView();
        numPores_ = gridView.size(dim);
        numThroats_ = gridView.size(0);
        totalBulkVolume_ = GridCreator::totalBulkVolume_;
        calculateInitialBulkPorosity_();
    }

    /*!
     * \brief Returns the bulk porosity
     */
    void printGridInfo() const
    {
        const auto name = [](GridType type)
        {
            switch(type)
            {
                case GridType::dgf:
                    return "dgf-file";
                case GridType::gmsh:
                    return "gmsh-file";
                case GridType::generic:
                    return "user-specified input";
                default:
                    return "unknown";
            }
        };

        const auto geometry = [](Shape s)
        {
            switch(s)
            {
                case Shape::SquarePrism:
                    return "SquarePrism";
                case Shape::CirclePrism:
                    return "CirclePrism";
                case Shape::TrianglePrism:
                    return "TrianglePrism";
                case Shape::Sphere:
                    return "Sphere";
                case Shape::RectanglePrism:
                    return "RectanglePrism";
                case Shape::EllipsePrism:
                    return "EllipsePrism";
                default:
                    return "unknown";
            }
        };

        std::cout << "Created porenetwork grid from " << name(gridType())
                  <<" with " << numPores_ << " pores and " << numThroats_ << " throats,"
                  <<" with total bulk volume " << totalBulkVolume_
                  << ", pore volume " << bulkPorosity_* totalBulkVolume_ << " (geometry is " << geometry(GET_PROP_VALUE(TypeTag, PoreGeometry)) << ")"
                  << ", porosity " << bulkPorosity_ << "." << std::endl;
    }

    /*!
     * \brief Returns the bulk porosity
     */
    Scalar bulkPorosity() const
    {
        return bulkPorosity_;
    }

    /*!
     * \brief Returns the total cumulative throat volume
     */
    Scalar totalBulkVolume() const
    {
        return totalBulkVolume_;
    }

    /*!
     * \brief Returns the type of the grid
     */
    auto gridType() const
    {
        return gridType_;
    }

private:

    /*!
     * \brief Calculates the bulk porosity
     *
     */
    void calculateInitialBulkPorosity_()
    {
        const auto& gridView = GridCreator::grid().leafGridView();

        const bool omitPoresOnBoundary = true; //TODO: introduce properties
        const bool considerPoreThroats = false;
        const unsigned int poreRadiusIdx = 0;
        const unsigned int boundaryIdx = 1;
        const unsigned int throatRadiusIdx = 0;
        const unsigned int throatLengthIdx = 1;

        std::vector<Scalar> poreVolume(numPores_);
        std::vector<int> poreLabel(numPores_);

        // get a vector of pore radii
        for(const auto &vertex : vertices(gridView))
        {
            const int vIdx = gridView.indexSet().index(vertex);

            poreLabel[vIdx] = GridCreator::parameters(vertex)[boundaryIdx];


            if(GET_PROP_VALUE(TypeTag, PoreGeometry) == Shape::SquarePrism)
                poreVolume[vIdx] = cubicPoreVolume(GridCreator::parameters(vertex)[poreRadiusIdx]);
            else if(GET_PROP_VALUE(TypeTag, PoreGeometry) == Shape::Sphere)
                poreVolume[vIdx] = sphericPoreVolume(GridCreator::parameters(vertex)[poreRadiusIdx]);
            else if(GET_PROP_VALUE(TypeTag, PoreGeometry) == Shape::TwoPlates)
                poreVolume[vIdx] = GridCreator::parameters(vertex)[poreRadiusIdx]*GridCreator::parameters(vertex)[poreRadiusIdx];
            else
                DUNE_THROW(Dune::InvalidStateException, "Invalid pore geometry");
        }
        if(!considerPoreThroats) //pore throats neglected
        {
            if(omitPoresOnBoundary)
                bulkPorosity_ = calculateBulkPorosity(totalBulkVolume_,
                                                      poreVolume, poreLabel);
            else
                bulkPorosity_ = calculateBulkPorosity(totalBulkVolume_, poreVolume);
        }
        else // pore throats considered
        {
            std::vector<Scalar> throatVolume(numThroats_);
            // get a vector of throat radii and lengths
            for(const auto &element : elements(gridView))
            {
                const int eIdx = gridView.indexSet().index(element);
                const Scalar throatLength = GridCreator::parameters(element)[throatLengthIdx];
                const Scalar throatRadius = GridCreator::parameters(element)[throatRadiusIdx];
                throatVolume[eIdx] = cylindricThroatVolume(throatLength, throatRadius);
            }
            if(omitPoresOnBoundary)
                bulkPorosity_ = calculateBulkPorosity(totalBulkVolume_, poreVolume,
                                                                   poreLabel,
                                                                   throatVolume);
            else
                bulkPorosity_ = calculateBulkPorosity(totalBulkVolume_, poreVolume,
                                                                   std::vector<int>(),
                                                                   throatVolume);
        }
    }

    Scalar bulkPorosity_;
    Scalar totalBulkVolume_;
    std::size_t numPores_;
    std::size_t numThroats_;
    GridType gridType_;
};

/*!
 * \brief Provides a grid creator builds a pore network grid from  a host grid
 */
template <class TypeTag>
class PoreNetworkGridCreator
{
    using Implementation = typename GET_PROP_TYPE(TypeTag, GridCreator);

    friend PNMGridCreatorParameterHelper<TypeTag>;
    friend Dumux::GridProperties<TypeTag>;

public:
    using Grid = typename GET_PROP_TYPE(TypeTag, Grid); //TODO improve this
    using GridView = typename Grid::LeafGridView;
private:

    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using PersistentParameterContainer = Dune::PersistentContainer<Grid, std::vector<Scalar>>;

    enum {
        dim = GridView::dimension,
        dimworld = GridView::dimensionworld
    };

    static_assert(dimworld == 2 || dimworld == 3, "DimWorld must be 2 or 3!");
    static_assert(dim == 1, "Dim must be 1!");

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;

    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;
    using ReferenceElements = typename Dune::ReferenceElements<Scalar, dimworld>;

    using ParameterHelper = Dumux::PNMGridCreatorParameterHelper<TypeTag>;
    using Input = Dumux::PNMGridCreatorInputHelper<TypeTag>;

    using BoundaryFaceMarker = std::array<int, 2*dimworld>;
    using PriorityList = std::array<int, dimworld>;

    using GridProperties = Dumux::GridProperties<TypeTag>;

public:

    enum ParamterIndex
    {
        poreRadiusIdx = 0,
        boundaryIdx = 1,
        throatRadiusIdx = 0,
        throatLengthIdx = 1,
        throatLabelIdx = 2
    };

    enum class GridType : unsigned int
    {dgf, gmsh, generic};

    /*!
     * \brief Return the grid properties
     */
    static auto gridProperties()
    {
        return gridProperties_;
    }

    /*!
     * \brief Make the amazing pnm grid
     */
    static void makeGrid(const std::string& modelParamGroup = "")
    {
        // First, try to create it from a dgf file in GridParameterGroup.File. The grid will be created as specified in the file.
        if(Input::parameterGiven("File"))
        {
            const auto file = getParamFromGroup<std::string>(modelParamGroup, "Grid.File");

            if(getFileExtension_(file) == "dgf")
            {
                type_ = GridType::dgf;
                Implementation::makeGridFromDgfFile_(file);
                gridProperties_.update();
                gridProperties_.printGridInfo();
                maybeWriteToDgf_(modelParamGroup);
                return;
            }
            else if(getFileExtension_(file) == "msh")
            {
#if !HAVE_UG
                DUNE_THROW(Dune::GridError, "Constructing grids from gmsh files requires UG grid!");
#else
                type_ = GridType::gmsh;
                Implementation::makeGridFromGmshFile_(file);
                Implementation::sanitizeGrid_();
                ParameterHelper::assignParameters();
                gridProperties_.update();
                gridProperties_.printGridInfo();
                maybeWriteToDgf_();
                return;
#endif
            }
            else
                DUNE_THROW(Dune::IOError, "Grid type " << Dune::className<Grid>() << " only supports DGF (*.dgf) and GMSH (*.msh) but the specified filename has extension: *."<< getFileExtension_(file));
        }
        else
        {
            // If no file is given, a host grid will be created according to some input parameters.
            // A pore network grid will be created based on the host grid (as given in input file) elements' edges
            try {
                    // make the grid and get some parameters
                    type_ = GridType::generic;
                    Implementation::makeGridFromFactory_(modelParamGroup);
                    Implementation::sanitizeGrid_(modelParamGroup);
                    ParameterHelper::assignParameters();
                    gridProperties_.update();
                    gridProperties_.printGridInfo();
                    maybeWriteToDgf_(modelParamGroup);
                    return;
            }
            catch (...) { throw; }
        }
    }

    /*!
     * \brief Call the parameters function
     *
     * \param entity The entity of interest (vertex or element)
     */
    template <class Entity>
    static const std::vector<Scalar>& parameters(const Entity& entity)
    {
        if(!(type_ == GridType::dgf))
        {
            if (int(Entity::codimension) == 0)
            {
                assert(elementParameters_ && "Element parameters are either not set or have already been deleted");
                assert(!(*elementParameters_)[entity].empty() && "No parameters available. Something might be wrong with your grid file!");
                return (*elementParameters_)[entity];
            }
            else if (int(Entity::codimension) == dim)
            {
                assert(vertexParameters_ && "Vertex parameters are either not set or have already been deleted");
                assert(!(*vertexParameters_)[entity].empty() && "No parameters available. Something might be wrong with your grid file!");
                return (*vertexParameters_)[entity];
            }
            else
                DUNE_THROW(Dune::NotImplemented, "Parameters for entity with codimension " << Entity::codimension);
        }
        else
        {
            assert(!dgfGridPtr().parameters(entity).empty() && "No parameters available. Something might be wrong with your grid file!");
            return dgfGridPtr().parameters(entity);
        }
    }

     /*!
     * \brief Computes and returns the a vector of coordination numbers.
     *        Forwards to helper class.
     *
     */
    static auto computeCoordinationNumbers()
    {
        return ParameterHelper::computeCoordinationNumbers();
    }

     /*!
     * \brief Deletes the vector of vertex parameters (when no DGF file is used). Use with care.
     *
     */
    static void deleteVertexParamterContainers()
    {
        if(type_ == GridType::dgf)
            DUNE_THROW(Dune::InvalidStateException, "Cannot delete DUNE's dgf param containers");

        vertexParameters_.reset();
    }

     /*!
     * \brief Deletes the vector of element parameters (when no DGF file is used). Use with care.
     *
     */
    static void deleteElementParamterContainers()
    {
        if(type_ == GridType::dgf)
            DUNE_THROW(Dune::InvalidStateException, "Cannot delete DUNE's dgf param containers");

        elementParameters_.reset();
    }

     /*!
     * \brief Computes and returns the label of a given throat.
     *        Forwards to helper class.
     *
     * \param element The element (throat)
     */
    static auto throatLabel(const Element &element)
    {
        return ParameterHelper::throatLabel(element);
    }

private:

    /*!
     * \brief Makes a grid from a DGF file. This is used by grid managers that only support DGF. No further processing on the grid takes place.
     */
    static void makeGridFromDgfFile_(const std::string& fileName)
    {
        // We found a file in the input file...does it have a supported extension?
        const std::string extension = getFileExtension_(fileName);
        if(extension != "dgf")
            DUNE_THROW(Dune::IOError, "Grid type " << Dune::className<Grid>() << " only supports DGF (*.dgf) but the specified filename has extension: *."<< extension);

        dgfGridPtr() = Dune::GridPtr<Grid>(fileName.c_str(), Dune::MPIHelper::getCommunicator());
        const auto& gridView = grid().leafGridView();

        // calculate the bounding box of the local partition of the grid view and the total bulk volume
        GlobalPosition bBoxMax(-std::numeric_limits<Scalar>::max());
        GlobalPosition bBoxMin( std::numeric_limits<Scalar>::max());
        for (const auto& vertex : vertices(gridView))
        {
            for (int i=0; i<dimworld; i++)
            {
                bBoxMin[i] = std::min(bBoxMin[i], vertex.geometry().corner(0)[i]);
                bBoxMax[i] = std::max(bBoxMax[i], vertex.geometry().corner(0)[i]);
            }
        }
        totalBulkVolume_ = 1.0;
        for(int i = 0; i < dimworld; ++i)
            totalBulkVolume_ *=  (bBoxMax[i] - bBoxMin[i]);
    }

#if HAVE_UG
    /*!
     * \brief Makes a grid based on a file using the element edges as pore throats. We currently support *.msh (Gmsh mesh format).
     */
    static void makeGridFromGmshFile_(const std::string& fileName)
    {
        using HostGrid = Dune::UGGrid<dimworld>;

        std::vector<int> elementMarkers, boundaryFacetMarkers;
        bool verbose = false;
        try { verbose = GET_RUNTIME_PARAM_FROM_GROUP_CSTRING(TypeTag, bool, GET_PROP_VALUE(TypeTag, GridParameterGroup).c_str(), Verbosity);}
        catch (Dumux::ParameterException &e) { }
        auto hostGrid = std::shared_ptr<HostGrid>(Dune::GmshReader<HostGrid>::read(fileName, boundaryFacetMarkers, elementMarkers, verbose, false));
        auto leafGridView = hostGrid->leafGridView();
        std::vector<int> hostGridPoreLabel;
        hostGridPoreLabel.resize(leafGridView.size(dimworld), -1);
        std::vector<unsigned int> vertexIdxToHostVertexIdx;
        for(const auto& element : elements(leafGridView))
        {
            totalBulkVolume_ += element.geometry().volume();
            for(const auto& intersection : intersections(leafGridView, element))
            {
                if(intersection.boundary())
                {
                    const auto& refElement = ReferenceElements::general(element.geometry().type());
                    // loop over vertices of the intersection facet
                    for(int vIdx = 0; vIdx < refElement.size(intersection.indexInInside(), 1, dimworld); vIdx++)
                    {
                        // get local vertex index with respect to the element
                        auto vIdxLocal = refElement.subEntity(intersection.indexInInside(), 1, vIdx, dimworld);
                        auto vIdxGlobal = leafGridView.indexSet().subIndex(element, vIdxLocal, dimworld);

                        // make sure we always take the lowest non-zero marker (problem dependent!)
                        if (hostGridPoreLabel[vIdxGlobal] == -1)
                            hostGridPoreLabel[vIdxGlobal] = boundaryFacetMarkers[intersection.boundarySegmentIndex()];
                        else
                        {
                            const auto mark = boundaryFacetMarkers[intersection.boundarySegmentIndex()];
                            if (hostGridPoreLabel[vIdxGlobal] < mark)
                                hostGridPoreLabel[vIdxGlobal] = mark;
                        }
                    }
                }
            }
        }
        // an array of scalars of size dimworld
        using Threshold = typename std::array<Scalar, dimworld>;

        // an epsilon for floating point comparisons
        const Scalar eps = std::numeric_limits<Scalar>::epsilon();

        // threshold
        Threshold threshold;
        std::fill(threshold.begin(), threshold.end(), 90.0);
        try { threshold =  GET_RUNTIME_PARAM_FROM_GROUP_CSTRING(TypeTag, Threshold, GET_PROP_VALUE(TypeTag, GridParameterGroup).c_str(), ThresholdAngle);}
        catch (Dumux::ParameterException &e) { }
        // check if the parameter is in the right range
        if(std::any_of(threshold.begin(), threshold.end(), [&eps](Scalar t){ return t > 90 + eps;}))
            DUNE_THROW(Dune::InvalidStateException, "Thresholds have to be chosen smaller than 90°!");

        // probability
        Threshold probability;
        std::fill(probability.begin(), probability.end(), 0.0);
        try { probability = GET_RUNTIME_PARAM_FROM_GROUP_CSTRING(TypeTag, Threshold, GET_PROP_VALUE(TypeTag, GridParameterGroup).c_str(), DeletionProbability);}
        catch (Dumux::ParameterException &e) { }
        // check if the parameter is in the right range
        if(std::any_of(probability.begin(), probability.end(), [&eps](Scalar p){ return p > 1.0 + eps || p < 0.0 - eps;}))
            DUNE_THROW(Dune::InvalidStateException, "Probabilities have to be between in the interval [0.0, 1.0]!");

        // TODO arbitrary axes
        std::array<GlobalPosition, dimworld> axes;
        if(dimworld == 3)
        {
            axes[0] = {1.0, 0.0, 0.0};
            axes[1] = {0.0, 1.0, 0.0};
            axes[2] = {0.0, 0.0, 1.0};
        }
        else if(dimworld == 2)
        {
            axes[0] = {1.0, 0.0};
            axes[1] = {0.0, 1.0};
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "dimworld must be > 1");

        // uniform random number generator for threshold checking
        std::random_device rd;
        std::mt19937 generator(rd());
        std::uniform_real_distribution<> uniformdist(0, 1);

        // extract the necessary information from the host grid
        std::vector<bool> vertexInserted(leafGridView.size(dimworld), false);
        std::vector<unsigned int> hostVertexIdxToVertexIdx(leafGridView.size(dimworld));
        std::vector<unsigned int> edgeInserted(leafGridView.size(dimworld-1), false);

        // new vertices and elements
        std::vector<GlobalPosition> vertexSet;
        std::vector<std::pair<unsigned int, unsigned int> > elementSet;

        // compute elements and vertexSet
        for (const auto& element : elements(hostGrid->leafGridView()))
        {
            auto geometry = element.geometry();
            const auto& refElement = ReferenceElements::general(geometry.type());

            // go over all edges and add them as elements if they passed all the tests
            for (unsigned int edgeIdx = 0; edgeIdx < element.subEntities(dimworld-1); ++edgeIdx)
            {
                unsigned int vIdxLocal1 = refElement.subEntity(edgeIdx, dimworld-1, 0, dimworld);
                unsigned int vIdxLocal2 = refElement.subEntity(edgeIdx, dimworld-1, 1, dimworld);
                unsigned int vIdxGlobal1 = leafGridView.indexSet().subIndex(element, vIdxLocal1, dimworld);
                unsigned int vIdxGlobal2 = leafGridView.indexSet().subIndex(element, vIdxLocal2, dimworld);
                unsigned int edgeIdxGlobal = leafGridView.indexSet().subIndex(element, edgeIdx, dimworld-1);

                // edge already checked?
                if (edgeInserted[edgeIdxGlobal])
                    continue;
                else
                    edgeInserted[edgeIdxGlobal] = true;

                // add vertexSet
                const auto corner1 = geometry.corner(vIdxLocal1);
                const auto corner2 = geometry.corner(vIdxLocal2);

                // remove connections on the boundary if desired
                bool skipElement = false;
                const auto removeThroatsOnBoundary = Input::removeThroatsOnBoundary();
                if (!removeThroatsOnBoundary.empty())
                    for (auto i : removeThroatsOnBoundary)
                        if (hostGridPoreLabel[vIdxGlobal1] == i && hostGridPoreLabel[vIdxGlobal2] == i)
                            skipElement = true;
                if (skipElement) continue;

                // pruning (to create anisotropy)
                const auto vector = corner1-corner2;
                // compute angles, if we are smaller than a threshold angle to a unit vector, do not
                // add this element. This results in a reduced permeability in the direction of that vector
                for (std::size_t i = 0; i < dimworld; ++i)
                {
                    // check if vector is in hourglass around axes[i] with angle threshold[i]
                    const Scalar angle = std::acos(axes[i] * vector / axes[i].two_norm() / vector.two_norm())*180.0/M_PI;
                    if (std::abs(angle) < threshold[i] || std::abs(angle-180) < threshold[i])
                    {
                        // keep the element with the probability given by the user
                        if (uniformdist(generator) < probability[i])
                        {
                            skipElement = true;
                            break;
                        }
                    }
                }

                if (skipElement) continue;

                // if we made it until here we can add the element and the vertices
                unsigned int newVertexIdx1, newVertexIdx2;
                if(vertexInserted[vIdxGlobal1])
                {
                    newVertexIdx1 = hostVertexIdxToVertexIdx[vIdxGlobal1];
                }
                else
                {
                    vertexInserted[vIdxGlobal1] = true;
                    newVertexIdx1 = vertexSet.size();
                    hostVertexIdxToVertexIdx[vIdxGlobal1] = newVertexIdx1;
                    vertexSet.push_back(corner1);
                    vertexIdxToHostVertexIdx.push_back(vIdxGlobal1);

                }
                if(vertexInserted[vIdxGlobal2])
                {
                    newVertexIdx2 = hostVertexIdxToVertexIdx[vIdxGlobal2];
                }
                else
                {
                    vertexInserted[vIdxGlobal2] = true;
                    newVertexIdx2 = vertexSet.size();
                    hostVertexIdxToVertexIdx[vIdxGlobal2] = newVertexIdx2;
                    vertexSet.push_back(corner2);
                    vertexIdxToHostVertexIdx.push_back(vIdxGlobal2);
                }
                elementSet.emplace_back(newVertexIdx1, newVertexIdx2);
            }
        }

        if (elementSet.empty())
            DUNE_THROW(Dune::GridError, "Trying to create pore network with zero elements!");

        // make the actual porenetwork grid
        Dune::GridFactory<Grid> factory;
        for (auto&& vertex : vertexSet)
            factory.insertVertex(vertex);
        for (auto&& element : elementSet)
            factory.insertElement(Dune::GeometryType(int(dim)), {element.first, element.second});

        gridPtr() = std::shared_ptr<Grid>(factory.createGrid());

        if (Input::outputHostGrid())
        {
            // construct a vtk output writer and attach the boundaryMarkers
            Dune::VTKSequenceWriter<typename HostGrid::LeafGridView> vtkWriter(leafGridView, "test", ".", "");
            vtkWriter.addVertexData(hostGridPoreLabel, "boundaryVertexMarker");
            vtkWriter.write(0);
        }
    }
#endif

    /*!
     * \brief Makes a generic grid based on user-input
     */
    static void makeGridFromFactory_(const std::string& modelParamGroup)
    {
        // The required parameters
        const GlobalPosition upperRight = getParamFromGroup<GlobalPosition>(modelParamGroup, "Grid.UpperRight");

        // The optional parameters (they have a default)
        GlobalPosition lowerLeft(0.0);
        try { lowerLeft = getParamFromGroup<GlobalPosition>(modelParamGroup, "Grid.LowerLeft"); }
        catch (Dumux::ParameterException &e) { }

        using CellArray = std::array<unsigned int, dimworld>;
        CellArray cells;
        std::fill(cells.begin(), cells.end(), 1);
        try { cells = getParamFromGroup<CellArray>(modelParamGroup, "Grid.Cells"); }
        catch (Dumux::ParameterException &e) { }

        // set probabilities for deleting a certain direction (for creating anisotropy)
        const int numDirections = (dimworld < 3) ? 4 : 13;
        std::vector<Scalar> directionProbability(numDirections,0.0);
        try {
                directionProbability = getParamFromGroup<std::vector<Scalar>>(modelParamGroup, "Grid.DeletionProbability");
                if(directionProbability.size() != numDirections)
                {
                            // define directions (to be used for user specified anisotropy)
                            Dune::FieldVector<std::string,numDirections> directions;
                            if(dimworld < 3) // 2D
                            {
                                // x, y
                                directions[0] = "1: (1, 0)\n";
                                directions[1] = "2: (0, 1)\n";
                                // diagonals through cell midpoint
                                directions[2] = "3: (1, 1)\n";
                                directions[3] = "4: (1, -1)\n";
                            }
                            else // 3D
                            {
                                // x, y, z
                                directions[0] = " 1: (1, 0, 0)\n";
                                directions[1] = "2: (0, 1, 0)\n";
                                directions[2] = "3: (0, 0, 1)\n";
                                //face diagonals
                                directions[3] = "4: (1, 1, 0)\n";
                                directions[4] = "5: (1, -1, 0)\n";
                                directions[5] = "6: (1, 0, 1)\n";
                                directions[6] = "7: (1, 0, -1)\n";
                                directions[7] = "8: (0, 1, 1)\n";
                                directions[8] = "9: (0, 1, -1)\n";
                                // diagonals through cell midpoint
                                directions[9] = "10: (1, 1, 1)\n";
                                directions[10] = "11: (1, 1, -1)\n";
                                directions[11] = "12: (-1, 1, 1)\n";
                                directions[12] = "13: (-1, -1, 1)\n";
                            }
                            DUNE_THROW(Dune::InvalidStateException, "You must specifiy probabilities for all directions (" << numDirections << ") \n" << directions << "\nExample (3D):\n\n"
                            << "DeletionProbability = 0.5 0.5 0 0 0 0 0 0 0 0 0 0 0 \n\n"
                            << "deletes approximately 50% of all throats in x and y direction, while no deletion in any other direction takes place.\n" );
                        }
                    }
                    catch (Dumux::ParameterException &e) { }

                    // create the host grid
                    using HostGrid = Dune::YaspGrid<dimworld, Dune::EquidistantOffsetCoordinates<Scalar, dimworld> >;
                    auto hostGrid = Dune::StructuredGridFactory<HostGrid>::createCubeGrid(lowerLeft, upperRight, cells);
                    auto leafGridView = hostGrid->leafGridView();

                    // set the priority which decides the order the vertices on the boundary are indexed
                    // by default, vertices on min/max faces in x direcetion have the highest priority, followed by y and z
                    std::vector<int> poreLabel;
                    poreLabel.resize(leafGridView.size(dimworld), -1);
                    for(const auto& element : elements(leafGridView))
                    {
                        totalBulkVolume_ += element.geometry().volume();
                        for(const auto& intersection : intersections(leafGridView, element))
                        {
                            if(intersection.boundary())
                            {
                                const auto& refElement = ReferenceElements::general(element.geometry().type());
                                // loop over vertices of the intersection facet
                                for(int vIdx = 0; vIdx < refElement.size(intersection.indexInInside(), 1, dimworld); vIdx++)
                                {
                                    // get local vertex index with respect to the element
                                    auto vIdxLocal = refElement.subEntity(intersection.indexInInside(), 1, vIdx, dimworld);
                                    auto vIdxGlobal = leafGridView.indexSet().subIndex(element, vIdxLocal, dimworld);
                                    auto vertex = element.template subEntity<dimworld>(vIdxLocal);
                                    auto globalPos = vertex.geometry().center();

                                    // make sure not to override already assigned markers
                                    if (poreLabel[vIdxGlobal] == -1)
                                    poreLabel[vIdxGlobal] = ParameterHelper::boundaryFaceMarkerAtPos(globalPos, lowerLeft, upperRight);
                                }
                            }
                        }
                    }

                    if (Input::outputHostGrid())
                    {
                        // construct a vtk output writer and attach the boundaryMarkers
                        Dune::VTKSequenceWriter<typename HostGrid::LeafGridView> vtkWriter(leafGridView, "test", ".", "");
                        vtkWriter.addVertexData(poreLabel, "boundaryVertexMarker");
                        vtkWriter.write(0);
                    }

                    // extract the necessary information from the host grid
                    std::vector<bool> vertexInserted(leafGridView.size(dimworld), false);
                    std::vector<unsigned int> hostVertexIdxToVertexIdx(leafGridView.size(dimworld));
                    std::vector<unsigned int> edgeInserted(leafGridView.size(dimworld-1), false);
                    std::vector<unsigned int> faceInserted(leafGridView.size(dimworld-2), false);

                    // new vertices and elements
                    std::vector<GlobalPosition> vertexSet;
                    std::vector<std::pair<unsigned int, unsigned int> > elementSet;

                    std::vector<unsigned int> vertexIdxToHostVertexIdx;

                    // compute elements and vertexSet
                    for (const auto& element : elements(leafGridView))
                    {
                        auto geometry = element.geometry();
                        const auto& refElement = ReferenceElements::general(geometry.type());

                        // lambda function for setting vertices and elements
                        // will be used both for setting elements on the hostgrid element's edges and diagonals
                        auto setVerticesAndElements = [&](unsigned int vIdxLocal1,  unsigned int vIdxLocal2, Scalar directionProbability)
                        {
                            std::random_device rd;
                            std::mt19937 generator(rd());
                            std::uniform_real_distribution<> uniformdist(0, 1);
                            // skip the element if the random number is below a threshold
                            if (uniformdist(generator) < directionProbability)
                            return;

                            typedef Dune::MultipleCodimMultipleGeomTypeMapper<typename HostGrid::LeafGridView> HostVertexMapper;
                            HostVertexMapper vertexMapper(leafGridView, Dune::mcmgVertexLayout());
                            const unsigned int vIdxGlobal1 = vertexMapper.subIndex(element, vIdxLocal1, dimworld);
                            const unsigned int vIdxGlobal2 = vertexMapper.subIndex(element, vIdxLocal2, dimworld);
                            unsigned int newVertexIdx1, newVertexIdx2;

                            if(vertexInserted[vIdxGlobal1])
                            {
                                newVertexIdx1 = hostVertexIdxToVertexIdx[vIdxGlobal1];
                            }
                            else
                            {
                                vertexInserted[vIdxGlobal1] = true;
                                newVertexIdx1 = vertexSet.size();
                                hostVertexIdxToVertexIdx[vIdxGlobal1] = newVertexIdx1;
                                const auto corner1 = geometry.corner(vIdxLocal1);
                                vertexSet.push_back(corner1);
                                vertexIdxToHostVertexIdx.push_back(vIdxGlobal1);

                            }
                            if(vertexInserted[vIdxGlobal2])
                            {
                                newVertexIdx2 = hostVertexIdxToVertexIdx[vIdxGlobal2];
                            }
                            else
                            {
                                vertexInserted[vIdxGlobal2] = true;
                                newVertexIdx2 = vertexSet.size();
                                hostVertexIdxToVertexIdx[vIdxGlobal2] = newVertexIdx2;
                                const auto corner2 = geometry.corner(vIdxLocal2);
                                vertexSet.push_back(corner2);
                                vertexIdxToHostVertexIdx.push_back(vIdxGlobal2);
                            }
                            elementSet.emplace_back(newVertexIdx1, newVertexIdx2);
                        };

                        // go over all edges and add them as elements if they passed all the tests
                        for (unsigned int edgeIdx = 0; edgeIdx < element.subEntities(dimworld-1); ++edgeIdx)
                        {
                            unsigned int vIdxLocal1 = refElement.subEntity(edgeIdx, dimworld-1, 0, dimworld);
                            unsigned int vIdxLocal2 = refElement.subEntity(edgeIdx, dimworld-1, 1, dimworld);
                            unsigned int edgeIdxGlobal = leafGridView.indexSet().subIndex(element, edgeIdx, dimworld-1);

                            // edge already checked?
                            if (edgeInserted[edgeIdxGlobal])
                            continue;
                            else
                            edgeInserted[edgeIdxGlobal] = true;

                            // remove connections on the boundary if desired TODO: find a better way to do this...
                            bool skipElement = false;
                            const auto removeThroatsOnBoundary = Input::removeThroatsOnBoundary();

                            if (!removeThroatsOnBoundary.empty())
                            {
                                const auto pos1 = geometry.corner(vIdxLocal1);
                                const auto pos2 = geometry.corner(vIdxLocal2);
                                auto center = (pos2 - pos1);
                                center *= 0.5;
                                center += pos1;

                                const Scalar eps = 1e-8; // TODO adaptive eps
                                for (auto i : removeThroatsOnBoundary)
                                {
                                    if (skipElement)
                                        continue;

                                    switch(i)
                                    {
                                        case 0: skipElement = center[0] < lowerLeft[0] + eps; break;
                                        case 1: skipElement = center[0] > upperRight[0] - eps; break;
                                        case 2: skipElement = center[1] < lowerLeft[1] + eps; break;
                                        case 3: skipElement = center[1] > upperRight[1] - eps; break;
                                        case 4: skipElement = center[2] < lowerLeft[2] + eps; break;
                                        case 5: skipElement = center[2] > upperRight[2] - eps; break;
                                    }
                                }
                            }

                            if (skipElement)
                                continue;

                            // pruning (to create anisotropy)
                            // assign numbers to different spatial directions
                            unsigned int directionNumber(0);
                            // 2D
                            if(dimworld < 3 )
                            {
                                // y-direction
                                if(edgeIdx < 2)
                                directionNumber = 0;
                                // x-direction
                                else
                                directionNumber = 1;
                            }
                            // 3D
                            else
                            {
                                // z-direction
                                if(edgeIdx < 4)
                                directionNumber = 2;
                                // y-direction
                                else if(edgeIdx == 4 || edgeIdx == 5 || edgeIdx == 8 || edgeIdx == 9)
                                directionNumber = 1;
                                // x-direction
                                else if(edgeIdx == 6 || edgeIdx == 7 || edgeIdx == 10 || edgeIdx == 11)
                                directionNumber = 0;
                            }
                            setVerticesAndElements(vIdxLocal1,  vIdxLocal2, directionProbability[directionNumber]);
                        }

                        // add diagonals manually:
                        std::vector<std::array<unsigned int, 3>> diagonalVertices;
                        if(dimworld == 3) // 3D
                        {
                            //set diagonals on host grid element faces
                            for (unsigned int faceIdx = 0; faceIdx < element.subEntities(dimworld-2); ++faceIdx)
                            {
                                const unsigned int faceIdxGlobal = leafGridView.indexSet().subIndex(element, faceIdx, dimworld-2);
                                // face already checked?
                                if (faceInserted[faceIdxGlobal])
                                continue;
                                else
                                faceInserted[faceIdxGlobal] = true;

                                // get local vertex indices
                                std::array<unsigned int, 4> vIdxLocal;
                                std::array<unsigned int, 4> vIdxGlobal;
                                for(int i = 0; i < 4; i++)
                                {
                                    vIdxLocal[i]  = refElement.subEntity(faceIdx, dimworld-2, i, dimworld);
                                    vIdxGlobal[i] = leafGridView.indexSet().subIndex(element, vIdxLocal[i], dimworld);
                                }

                                // remove connections on the boundary if desired
                                bool skipElement = false;
                                const auto removeThroatsOnBoundary = Input::removeThroatsOnBoundary();
                                if (!removeThroatsOnBoundary.empty())
                                for (auto i : removeThroatsOnBoundary)
                                if (poreLabel[vIdxGlobal[0]] == i && poreLabel[vIdxGlobal[3]] == i)
                                skipElement = true;
                                if (skipElement) continue;

                                // pruning (to create anisotropy)
                                // assign numbers to different spatial directions
                                unsigned int directionNumber1, directionNumber2;
                                if(faceIdx < 2)
                                {
                                    directionNumber1 = 8;
                                    directionNumber2 = 7;
                                }
                                else if(faceIdx < 4)
                                {
                                    directionNumber1 = 6;
                                    directionNumber2 = 5;
                                }
                                else
                                {
                                    directionNumber1 = 4;
                                    directionNumber2 = 3;
                                }
                                diagonalVertices.push_back({{vIdxLocal[1],vIdxLocal[2], directionNumber1}});
                                diagonalVertices.push_back({{vIdxLocal[0], vIdxLocal[3], directionNumber2}});
                            }
                            diagonalVertices.push_back({{0, 7, 9}});
                            diagonalVertices.push_back({{3, 4, 10}});
                            diagonalVertices.push_back({{1, 6, 11}});
                            diagonalVertices.push_back({{2, 5, 12}});
                        }

                        if(dimworld < 3) // 2D
                        {
                            diagonalVertices.push_back({{0,3,2}});
                            diagonalVertices.push_back({{1,2,3}});
                        }

                        // insert all diagonals into the pore network grid
                        for (const auto& i : diagonalVertices)
                        {
                            auto vIdxLocal1 = i[0];
                            auto vIdxLocal2 = i[1];
                            auto directionNumber = i[2];
                            setVerticesAndElements(vIdxLocal1, vIdxLocal2, directionProbability[directionNumber]);
                        }
                    }

                    if (elementSet.empty())
                    DUNE_THROW(Dune::GridError, "Trying to create pore network with zero elements!");

                    // make the actual porenetwork grid
                    Dune::GridFactory<Grid> factory;
                    for (auto&& vertex : vertexSet)
                    factory.insertVertex(vertex);
                    for (auto&& element : elementSet)
                    factory.insertElement(Dune::GeometryType(int(dim)), {element.first, element.second});

                    gridPtr() = std::shared_ptr<Grid>(factory.createGrid());

                    vertexParameters_ = ParameterHelper::makeVertexParamContainer(grid(), 2);
                    elementParameters_ = ParameterHelper::makeElementParamContainer(grid(), 3);

                    const auto gridView = gridPtr()->leafGridView();
                    for(const auto &vertex :vertices(gridView))
                    {
                        const auto vIdxGlobal = gridView.indexSet().index(vertex);
                        (*vertexParameters_)[vertex][boundaryIdx] = poreLabel[vertexIdxToHostVertexIdx[vIdxGlobal]];
                    }
    }

    /*!
     * \brief post-processing to remove insular groups of elements that are not connected to a Dirichlet boundary
     */
    static void sanitizeGrid_(const std::string& modelParamGroup)
    {

        const auto pruningSeedIndices = getParamFromGroup<std::vector<int>>(modelParamGroup, "Grid.PruningSeedIndices", std::vector<int>{1});

        auto& vertexParameters = *vertexParameters_;
        auto& elementParameters = *elementParameters_;

        const auto gridView = grid().leafGridView();
        // pruning -- remove elements not connected to a Dirichlet boundary (marker == 1)
        std::vector<bool> elementIsConnected(gridView.size(0), false);

        for (const auto& element : elements(gridView))
        {
            auto eIdx = gridView.indexSet().index(element);
            if (elementIsConnected[eIdx])
                continue;

            bool isSeed = false;
            for (const auto& intersection : intersections(gridView, element))
            {
                auto vertex = element.template subEntity<dim>(intersection.indexInInside());
                // seed found
                if (std::any_of(pruningSeedIndices.begin(), pruningSeedIndices.end(), [&]( const int i ){ return i == vertexParameters[vertex][boundaryIdx]; }))
                {
                    isSeed = true;
                    break;
                }
            }

            if (isSeed)
            {
                elementIsConnected[eIdx] = true;

                // use iteration instead of recursion here because the recursion can get too deep
                std::stack<Element> elementStack;
                elementStack.push(element);
                while (!elementStack.empty())
                {
                    auto e = elementStack.top();
                    elementStack.pop();
                    for (const auto& intersection : intersections(gridView, e))
                    {
                        if (!intersection.boundary())
                        {
                            auto outside = intersection.outside();
                            auto nIdx = gridView.indexSet().index(outside);
                            if (!elementIsConnected[nIdx])
                            {
                                elementIsConnected[nIdx] = true;
                                elementStack.push(outside);
                            }
                        }
                    }
                }
            }
        }

        if(std::none_of(elementIsConnected.begin(), elementIsConnected.end(), [](const bool i){return i;}))
            DUNE_THROW(Dune::InvalidStateException, "sanitize() deleted all elements! Check your boundary face indices. "
                << "If the problem persisits, add at least one of your boundary face indices to PruningSeedIndices");

        // Remove the elements in the grid
        grid().preGrow();
        for (const auto& element : elements(gridView))
        {
            auto eIdx = gridView.indexSet().index(element);
            if (!elementIsConnected[eIdx])
            {
                grid().removeElement(element);
            }
        }
        // Triggers the grid growth process
        grid().grow();
        grid().postGrow();

        // resize the parameters
        vertexParameters.resize();
        elementParameters.resize();
        vertexParameters.shrinkToFit();
        elementParameters.shrinkToFit();
    }

    /*!
     * \brief Returns the filename extension of a given filename
     */
    static std::string getFileExtension_(const std::string& fileName)
    {
        std::size_t i = fileName.rfind('.', fileName.length());
        if (i != std::string::npos)
            return (fileName.substr(i+1, fileName.length() - i));
        else
            DUNE_THROW(Dune::IOError, "Please provide and extension for your grid file ('"<< fileName << "')!");
    }

    /*!
     * \brief Returns the filename extension of a given filename
     */
    static std::string getFileStem_(const std::string& fileName)
    {
        std::size_t i = fileName.rfind('.', fileName.length());
        if (i != std::string::npos)
            return (fileName.substr(0, i));
        else
            DUNE_THROW(Dune::IOError, "Please provide and extension for your grid file ('"<< fileName << "')!");
    }

    /*!
     * \brief Write foam-grid to dgf file
     */
    static void maybeWriteToDgf_(const std::string& modelParamGroup)
    {
        bool writeDgf = false;
        if(Input::parameterGiven("WriteDgfFile"))
            writeDgf = getParamFromGroup<bool>(modelParamGroup, "Grid.WriteDgfFile");
        if(!writeDgf)
            return;


        using FoamGridVertexMapper = Dune::MultipleCodimMultipleGeomTypeMapper<typename Grid::LeafGridView>;
        using VertexInfo = Dune::FieldVector<Scalar, dimworld+2>;

        const auto fileName = [&]()
        {
            if(gridProperties().gridType() == GridType::generic)
            {
                const auto outPutName = getParamFromGroup<std::string>(modelParamGroup, "Grid.FileName", "generic-pnm-grid.dgf");
                return outPutName;
            }
            else
            {
                const auto inputFile = getParamFromGroup<std::string>(modelParamGroup, "Grid.File");
                const auto inPutName = getFileStem_(inputFile);
                const auto outPutName = getParamFromGroup<std::string>(modelParamGroup, "Grid.FileName", inPutName + ".dgf");
                return outPutName;
            }
        }();

        struct ConnectionInfo
        {
            int vIdx1;
            int vIdx2;
            double radius;
            double length;
        };

        std::ofstream dgfFile;
        std::vector<VertexInfo> vertexInfos;
        std::vector<ConnectionInfo> connections;
        auto gridView = grid().leafGridView();
        dgfFile.open(fileName);
        dgfFile << "DGF\nVertex % Coordinates, volumes and boundary flags of the pore bodies\nparameters 2\n";

        for(const auto& vertex : vertices(gridView))
        {
            auto pos = vertex.geometry().center();
            auto params = parameters(vertex);
            auto vertInfo = {pos[0], pos[1], pos[2], params[0], params[1]};
            vertexInfos.push_back(vertInfo);
        }

        for(const auto& element : elements(gridView))
        {
            ConnectionInfo connection;
            FoamGridVertexMapper vertexMapper(gridView, Dune::mcmgVertexLayout());
            auto params = parameters(element);
            connection.vIdx1 = vertexMapper.subIndex(element, 0, dim);
            connection.vIdx2 = vertexMapper.subIndex(element, 1, dim);
            connection.radius = params[0];
            connection.length = params[1];
            connections.push_back(connection);
        }

        for(auto&& vertex : vertexInfos)
            dgfFile << vertex << std::endl;
        dgfFile << "#\nSIMPLEX % Connections of the pore bodies (pore throats)\nparameters 2\n";
        for(auto connection : connections)
            dgfFile << connection.vIdx1 << " " << connection.vIdx2 << " "  << connection.radius << " " << connection.length << std::endl;
        dgfFile << "#";
        dgfFile.close();
    }

public:

     /*!
     * \brief Returns a reference to the grid pointer (std::shared_ptr<Grid>)
     */
    static std::shared_ptr<Grid> &gridPtr()
    {
        static std::shared_ptr<Grid> gridPtr_;
        return gridPtr_;
    }

    /*!
     * \brief Returns a reference to the DGF grid pointer (Dune::GridPtr<Grid>).
     */
    static Dune::GridPtr<Grid> &dgfGridPtr()
    {
        if(type_ == GridType::dgf)
        {
            static Dune::GridPtr<Grid> dgfGridPtr_;
            return dgfGridPtr_;
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "The DGF grid pointer is only available if the grid was constructed with a DGF file!");
    }

    /*!
     * \brief Returns a reference to the grid.
     */
    static Grid &grid()
    {
        if(type_ == GridType::dgf)
            return *dgfGridPtr();
        else
            return *gridPtr();
    }

    /*!
     * \brief Distributes the grid on all processes of a parallel
     *        computation.
     */
    static void loadBalance()
    {
        if(type_ == GridType::dgf)
            dgfGridPtr().loadBalance();
        else
            gridPtr()->loadBalance();
    }

protected:

    static Scalar totalBulkVolume_;
    static std::unique_ptr<PersistentParameterContainer> vertexParameters_;
    static std::unique_ptr<PersistentParameterContainer> elementParameters_;
    static GridProperties gridProperties_;
    static GridType type_;
};

// Initialization of private variables

template <class TypeTag>
typename Dumux::GridProperties<TypeTag> PoreNetworkGridCreator<TypeTag>::gridProperties_ ;

template <class TypeTag>
typename Dumux::PoreNetworkGridCreator<TypeTag>::GridType PoreNetworkGridCreator<TypeTag>::type_ ;

template <class TypeTag>
std::unique_ptr<Dune::PersistentContainer<typename GET_PROP_TYPE(TypeTag, Grid), std::vector<typename GET_PROP_TYPE(TypeTag, Scalar)>>>
PoreNetworkGridCreator<TypeTag>::vertexParameters_;

template <class TypeTag>
std::unique_ptr<Dune::PersistentContainer<typename GET_PROP_TYPE(TypeTag, Grid), std::vector<typename GET_PROP_TYPE(TypeTag, Scalar)>>>
PoreNetworkGridCreator<TypeTag>::elementParameters_;

template <class TypeTag>
typename GET_PROP_TYPE(TypeTag, Scalar) PoreNetworkGridCreator<TypeTag>::totalBulkVolume_ = 0.0;

} // namespace Dumux

#endif
