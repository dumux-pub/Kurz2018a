// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup SpatialParameters
 * \brief The base class for spatial parameters for pore network models.
 */
#ifndef DUMUX_PNM_SPATIAL_PARAMS_1P_HH
#define DUMUX_PNM_SPATIAL_PARAMS_1P_HH

#include <dumux/common/properties.hh>
#include <dumux/porenetworkflow/common/geometry.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class PNMSpatialParams;

// forward declation of property tags
namespace Properties
{
NEW_TYPE_TAG(PNMSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(PNMSpatialParams, SpatialParams, Dumux::PNMSpatialParams<TypeTag>);
}

/*!
 * \ingroup SpatialParameters
 */

/**
 * \brief The base class for spatial parameters for pore network models.
 */
template<class TypeTag>
class PNMSpatialParams
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Implementation = typename GET_PROP_TYPE(TypeTag, SpatialParams);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);

    using Labels = typename GET_PROP_TYPE(TypeTag, Labels);

    using Element = typename GridView::template Codim<0>::Entity;
    using CoordScalar = typename GridView::ctype;

    static const int dim = GridView::dimension;
    static const int dimWorld = GridView::dimensionworld;
    using DimWorldMatrix = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>;
    using GlobalPosition = Dune::FieldVector<CoordScalar,dimWorld>;
    using GridCreator = typename GET_PROP_TYPE(TypeTag, GridCreator);

public:
    using PermeabilityType = Scalar;

    PNMSpatialParams(const Problem& problem)
    : problem_(problem), gridView_(problem.fvGridGeometry().gridView()), useBoundaryDataFromSpatialParams_(true), eps_(1e-6)
    {
        boundaryDefinition_ = getParamFromGroup<std::string>(GET_PROP_VALUE(TypeTag, ModelParameterGroup), "Grid.BoundaryDefinition", "Bbox");

        asImp_().getVertexParamters_();
        asImp_().getElementParamters_();

        if(boundaryDefinition_ == "IntersectionIterator")
            useBoundaryDataFromSpatialParams_ = false;

        updateBoundaryFactor();
    }

    ~PNMSpatialParams() {}

    /*!
     * \brief Called by the Problem to initialize the spatial params.
     */
    void init()
    {}

    /*!
     * \brief Harmonic average of a discontinuous scalar field at discontinuity interface
     *        (for compatibility reasons with the function below)
     * \return the averaged scalar
     * \param T1 first scalar parameter
     * \param T2 second scalar parameter
     * \param normal The unit normal vector of the interface
     */
    Scalar harmonicMean(const Scalar T1,
                        const Scalar T2,
                        const GlobalPosition& normal) const
    { return Dumux::harmonicMean(T1, T2); }

    /*************************** Pore Label *************************************************/
    /*!
     * \brief Returns the boundary pore index
     *
     * \param scv The sub control volume
     */
    int poreLabel(const SubControlVolume &scv) const
    {
        return poreLabel_[scv.dofIndex()];
    }

    /*!
     * \brief Returns the boundary pore index.
     *
     * \param dofIdxGlobal The global dof index of the pore
     */
    int poreLabel(const int dofIdxGlobal) const
    {
        return poreLabel_[dofIdxGlobal];
    }
    /****************************************************************************************/

    /*************************** Pore Radius ************************************************/
    /*!
     * \brief Function for defining the pore radius.
     *
     * \return radius
     * \param vertex The vertex
     */
    Scalar poreRadius(const SubControlVolume &scv, const VolumeVariables& volVars) const
    {
        return poreRadius_[scv.dofIndex()];
    }

    /*!
     * \brief Function for defining the pore radius.
     *
     * \return radius
     * \param vertex The vertex
     */
    Scalar poreRadius(const SubControlVolume &scv) const
    {
        return poreRadius_[scv.dofIndex()];
    }

    /*!
     * \brief Function for defining the pore radius.
     *
     * \return radius
     * \param dofIdxGlobal The global dof index of the pore
     * \note Overload this method to make it solution dependend
     */
    Scalar poreRadius(const int dofIdxGlobal, const VolumeVariables& volVars) const
    {
        return poreRadius_[dofIdxGlobal];
    }

    /*!
     * \brief Function for defining the pore radius.
     *
     * \return radius
     * \param dofIdxGlobal The global dof index of the pore
     * \note Overload this method to make it solution dependend
     */
    Scalar poreRadius(const int dofIdxGlobal) const
    {
        return poreRadius_[dofIdxGlobal];
    }

    /*!
     * \brief Function for defining the pore radius.
     *
     * \return radius
     * \param vertex The vertex
     */
    Scalar initialPoreRadius(const SubControlVolume &scv) const
    {
        return poreRadius_[scv.dofIndex()];
    }

    /*!
     * \brief Function for defining the pore radius.
     *
     * \return radius
     * \param dofIdxGlobal The global dof index of the pore
     * \note Overload this method to make it solution dependend
     */
    Scalar initialPoreRadius(const int dofIdxGlobal) const
    {
        return poreRadius_[dofIdxGlobal];
    }

    /****************************************************************************************/

    /*************************** Pore Volume ************************************************/
    /*!
     * \brief Returns the pore volume.
     *
     * \param scv The sub control volume
     */
    Scalar poreVolume(const SubControlVolume &scv, const VolumeVariables& volVars) const
    {
        return initialPoreVolume(scv.dofIndex());
    }

    /*!
     * \brief Returns the pore volume.
     *
     * \param scv The sub control volume
     */
    Scalar poreVolume(const SubControlVolume &scv) const
    {
        return initialPoreVolume(scv.dofIndex());
    }

    /*!
     * \brief Returns the pore volume.
     *
     * \param vIdx The vertex index
     */
    Scalar poreVolume(const int dofIdxGlobal, const VolumeVariables& volVars) const
    {
        return initialPoreVolume(dofIdxGlobal);
    }

    /*!
     * \brief Returns the pore volume.
     *
     * \param vIdx The vertex index
     */
    Scalar poreVolume(const int dofIdxGlobal) const
    {
        return initialPoreVolume(dofIdxGlobal);
    }

    /*!
     * \brief Returns the pore volume.
     *
     * \param scv The sub control volume
     */
    Scalar initialPoreVolume(const SubControlVolume &scv) const
    {
        return initialPoreVolume(scv.dofIndex());
    }


    /*!
     * \brief Returns the pore volume.
     *
     * \param vIdx The vertex index
     */
    Scalar initialPoreVolume(const int dofIdxGlobal) const
    {
        if(GET_PROP_VALUE(TypeTag, PoreGeometry) == Shape::SquarePrism)
            return cubicPoreVolume(poreRadius(dofIdxGlobal))*boundaryFactor(dofIdxGlobal);
        else if(GET_PROP_VALUE(TypeTag, PoreGeometry) == Shape::Sphere)
            return sphericPoreVolume(poreRadius(dofIdxGlobal))*boundaryFactor(dofIdxGlobal);
        else if(GET_PROP_VALUE(TypeTag, PoreGeometry) == Shape::TwoPlates)
            return poreRadius(dofIdxGlobal)*poreRadius(dofIdxGlobal)*boundaryFactor(dofIdxGlobal);
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid pore geometry");
    }

            /*!
    * \brief Compute / update the flux variables
    *
    * \param problem The problem
    */
   void updateBoundaryFactor()
   {
       const auto& fvGridGeometry = problem().fvGridGeometry();
       factor_.resize(fvGridGeometry.numScv(), 1.0);

       // Decrease the transmissibility (and thereby the fluxes) by a factor of
       // 0.5 for pores on the boundary and by  a factor of 0.25 for pores in corners.
       // This is important for the coupling with box bulk domains. Otherwise, bulk scvs at the
       // boundaries or at corners would receive too much mass flow.
       // Using this factor, a homogeneous pressure distribution (for symmetrical configurations)
       // can be realized.
       const Scalar eps = 1e-8;

       // get the y and z bounding "box"
       const auto yMin = fvGridGeometry.bBoxMin()[1];
       const auto yMax = fvGridGeometry.bBoxMax()[1];
       const auto zMin = fvGridGeometry.bBoxMin()[2];
       const auto zMax = fvGridGeometry.bBoxMax()[2];

       for(const auto& element : elements(fvGridGeometry.gridView())){
       auto fvGeometry = localView(problem().fvGridGeometry());
       fvGeometry.bind(element);
       for (const auto& scv : scvs(fvGeometry))
       {
        const auto dofIdxGlobal = scv.dofIndex();

           // get the y and z position of the element
           const auto y = scv.center()[1];
           const auto z = scv.center()[2];

           // treat pores on the boundary
           if((y < yMin + eps || y > yMax - eps || z < zMin + eps || z > zMax - eps) && factor_[dofIdxGlobal]==1)
               factor_[dofIdxGlobal] *= 0.5;

           // treat pores at the corners
           if((y < yMin + eps || y > yMax - eps) && (z < zMin + eps || z > zMax - eps) && (factor_[dofIdxGlobal]==0.5))
               factor_[dofIdxGlobal] *= 0.5;
       }
   }
   }

       Scalar boundaryFactor(int dofIdxGlobal) const
    {
        return factor_[dofIdxGlobal];
    }

    /****************************************************************************************/

    /*************************** Throat Label ***********************************************/
    /*!
     * \brief Returns an index indicating if a throat is touching the domain boundary.
     *
     * \param element The throat
     */
    int throatLabel(const Element &element) const
    {
        const int eIdx = gridView_.indexSet().index(element);
        return throatLabel_[eIdx];
    }

    /*!
     * \brief Returns an index indicating if a throat is touching the domain boundary.
     *
     * \param eIdx The throat index
     */
    int throatLabel(const int eIdx) const
    {
        return throatLabel_[eIdx];
    }
    /****************************************************************************************/

    /*************************** Throat Radius **********************************************/
    /*!
     * \brief Returns the radius of the throat
     *
     * \param scv The element. This represents a throat.
     */
    Scalar throatRadius(const Element& element, const ElementVolumeVariables& elemVolVars) const
    {
        const int eIdx = gridView_.indexSet().index(element);
        return throatRadius_[eIdx];
    }

    /*!
     * \brief Returns the radius of the throat
     *
     * \param scv The element. This represents a throat.
     */
    Scalar throatRadius(const Element& element) const
    {
        const int eIdx = gridView_.indexSet().index(element);
        return throatRadius_[eIdx];
    }

    /*!
     * \brief Returns the radius of the throat
     *
     * \param scv The element. This represents a throat.
     */
    Scalar throatRadius(const int eIdx) const
    {
        return throatRadius_[eIdx];
    }

    /*!
     * \brief Returns the radius of the throat
     *
     * \param scv The element. This represents a throat.
     */
    Scalar throatRadius(const int eIdx, const ElementVolumeVariables& elemVolVars) const
    {
        return throatRadius_[eIdx];
    }

    /*!
     * \brief Returns the radius of the throat
     *
     * \param scv The element. This represents a throat.
     */
    Scalar initialThroatRadius(const Element& element) const
    {
        const int eIdx = gridView_.indexSet().index(element);
        return throatRadius_[eIdx];
    }

    /*!
     * \brief Returns the radius of the throat
     *
     * \param scv The element. This represents a throat.
     */
    Scalar initialThroatRadius(const int eIdx) const
    {
        return throatRadius_[eIdx];
    }


    /****************************************************************************************/

    /*************************** Throat Length **********************************************/
    /*!
     * \brief Returns the length of the throat.
     *
     * \param scv The element. This represents a throat
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend
     */
    Scalar throatLength(const Element& element, const ElementVolumeVariables& elemVolVars) const
    {
        const int eIdx = gridView_.indexSet().index(element);
        return throatLength_[eIdx];
    }

    /*!
     * \brief Returns the length of the throat.
     *
     * \param scv The element. This represents a throat
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend
     */
    Scalar throatLength(const Element& element) const
    {
        const int eIdx = gridView_.indexSet().index(element);
        return throatLength_[eIdx];
    }

    /*!
     * \brief Returns the throat lenght
     *
     * \param element The element index
     */
    Scalar throatLength(const int eIdx) const
    {
        return throatLength_[eIdx];
    }

    /*!
     * \brief Returns the length of the throat.
     *
     * \param scv The element. This represents a throat
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend
     */
    Scalar initialThroatLength(const Element& element, const ElementVolumeVariables& elemVolVars) const
    {
        const int eIdx = gridView_.indexSet().index(element);
        return throatLength_[eIdx];
    }

    /*!
     * \brief Returns the length of the throat.
     *
     * \param scv The element. This represents a throat
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend
     */
    Scalar initialThroatLength(const Element& element) const
    {
        const int eIdx = gridView_.indexSet().index(element);
        return throatLength_[eIdx];
    }

    /*!
     * \brief Returns the throat lenght
     *
     * \param element The element index
     */
    Scalar initialThroatLength(const int eIdx) const
    {
        return throatLength_[eIdx];
    }
    /****************************************************************************************/

    /*************************** Throat Cross Section ***************************************/
    /*!
     * \brief Returns the throat cross-sectional Area
     *
     * \param element The element.
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend.
     */
    Scalar throatCrossSection(const Element& element, const ElementVolumeVariables& elemVolVars, const int phaseIdx = 0) const
    {
        const int eIdx = gridView_.indexSet().index(element);
        return initialThroatCrossSection(eIdx);
    }

    /*!
     * \brief Returns the throat cross-sectional Area
     *
     * \param element The element.
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend.
     */
    Scalar throatCrossSection(const Element& element, const int phaseIdx = 0) const
    {
        const int eIdx = gridView_.indexSet().index(element);
        return initialThroatCrossSection(eIdx);
    }

    /*!
     * \brief Returns the throat cross-sectional Area
     *
     * \param eIdx The element index.
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend.
     */
    Scalar throatCrossSection(const int eIdx) const
    {
        return initialThroatCrossSection(eIdx);
    }

    /*!
     * \brief Returns the throat cross-sectional Area
     *
     * \param element The element.
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend.
     */
    Scalar initialThroatCrossSection(const Element& element, const ElementVolumeVariables& elemVolVars) const
    {
        int eIdx = gridView_.indexSet().index(element);
        return initialThroatCrossSection(eIdx);
    }

    /*!
     * \brief Returns the throat cross-sectional Area
     *
     * \param element The element.
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend.
     */
    Scalar initialThroatCrossSection(const Element& element) const
    {
        int eIdx = gridView_.indexSet().index(element);
        return initialThroatCrossSection(eIdx);
    }

    /*!
     * \brief Returns the throat cross-sectional Area
     *
     * \param eIdx The element index.
     * \param elemVolVars The element volume variables.
     * \note Overload this method to make it solution dependend.
     */
    Scalar initialThroatCrossSection(const int eIdx) const
    {
        const Scalar r = throatRadius_[eIdx];
        if(GET_PROP_VALUE(TypeTag, ThroatGeometry) == Shape::SquarePrism)
            return 4*r*r;
        else if(GET_PROP_VALUE(TypeTag, ThroatGeometry) == Shape::CirclePrism)
            return M_PI*r*r;
        else if(GET_PROP_VALUE(TypeTag, ThroatGeometry) == Shape::TwoPlates)
            return 2*r;
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid throat geometry");
    }

    /****************************************************************************************/

    /*!
     * \brief Returns whether boundary information from the spatialParams should be used for the model's
     *        onBoundary() function
     */
    const bool useBoundaryDataFromSpatialParams() const
    { return useBoundaryDataFromSpatialParams_; }

    /*!
     * \brief Return the number of adjacent pore throat for each pore body
     *
     * \param vIdx The vertex index
     */
    const int numAdjacentThroats(const int dofIdxGlobal) const
    {
        return numAdjacentThroats_[dofIdxGlobal];
    }

    /*! \brief Define the porosity in [-].
    *
    * \param element The finite element
    * \param fvGeometry The finite volume geometry
    * \param scvIdx The local index of the sub-control volume where
    */
    Scalar porosity(const Element &element,
                    const SubControlVolume& scv,
                    const VolumeVariables& volVars) const
    { return 1.0; }

    /*!
     * \brief Returns whether a pore serves as inlet
     *
     * \param vertex The vertex
     */
    const bool isInletPore(const SubControlVolume &scv) const
    {
        const int vIdx = scv.dofIndex();
        return isInletPore(vIdx);
    }

    /*!
     * \brief Returns whether a pore serves as inlet
     *
     * \param element The element
     */
    const bool isInletPore(const int dofIdxGlobal) const
    {
        return poreLabel_[dofIdxGlobal] == Labels::inlet;
    }

    /*!
     * \brief Returns whether a pore serves as outlet
     *
     * \param vertex The vertex
     */
    const bool isOutletPore(const SubControlVolume &scv) const
    {
        const int vIdx = scv.dofIndex();
        return isOutletPore(vIdx);
    }

    /*!
     * \brief Returns whether a pore serves as outlet
     *
     * \param element The element
     */
    const bool isOutletPore(const int dofIdxGlobal) const
    {
        return poreLabel_[dofIdxGlobal] == Labels::outlet;
    }

    /*!
     * \brief Returns whether a throat serves as inlet
     *
     * \param element The element
     */
    const bool isInletThroat(const Element &element) const
    {
        const int eIdx = gridView_.indexSet().index(element);
        return throatLabel_[eIdx] == Labels::inlet;
    }

    /*!
     * \brief Returns whether a throat serves as inlet
     *
     * \param eIdx The element index
     */
    const bool isInletThroat(const int eIdx) const
    {
        return throatLabel_[eIdx] == Labels::inlet;
    }

    /*!
     * \brief Returns whether a throat serves as outlet
     *
     * \param element The element
     */
    const bool isOutletThroat(const Element &element) const
    {
        const int eIdx = gridView_.indexSet().index(element);
        return throatLabel_[eIdx] == Labels::outlet;
    }

    /*!
     * \brief Returns whether a throat serves as outlet
     *
     * \param eIdx The element index
     */
    const bool isOutletThroat(const int eIdx) const
    {
        return throatLabel_[eIdx] == Labels::outlet;
    }

    /*!
     * \brief Returns a reference to the gridview
     *
     */
    const GridView &gridView() const
    {
        return gridView_;
    }


     /*!
     * \brief Returns the minumum pore radius
     *
     */
    const Scalar minPoreRadius() const
    {
        return *std::min_element(poreRadius_.begin(), poreRadius_.end());
    }

     /*!
     * \brief Returns the maximum pore radius
     *
     */
    const Scalar maxPoreRadius() const
    {
        return *std::max_element(poreRadius_.begin(), poreRadius_.end());
    }

    /*!
     *  \brief Define the porosity \f$[-]\f$
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    template<class ElementSolutionVector>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolutionVector& elemSol) const
    { return 1.0; }


     /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
     *  on the position in the domain
     *
     *  \param element The finite volume element
     *  \param scv The sub-control volume
     *
     *  Solution dependent permeability function
     */
    template<class ElementSolutionVector>
    Scalar permeability(const Element& element,
                        const SubControlVolume& scv,
                        const ElementSolutionVector& elemSol) const
    { return 1.0; }


        /*!
     * \brief Returns the heat capacity \f$[J / (kg K)]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param globalPos The position of the center of the element
     *
     *TODO this is a themporary fix because the implicit VolumeVariables need that.
     * We have a different non-isothermal localresidual so that this is never used because
     * we do not have heat storage in the matrix
     */
    template<class ElementSolutionVector>
    Scalar solidHeatCapacity(const Element &element,
                             const SubControlVolume& scv,
                             const ElementSolutionVector &elemSol) const
    {
        return 0;
    }

    /*!
     * \brief Returns the mass density \f$[kg / m^3]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param globalPos The position of the center of the element
     *TODO this is a themporary fix because the implicit VolumeVariables need that.
     * We have a different non-isothermal localresidual so that this is never used because
     * we do not have heat storage in the matrix
     */
    template<class ElementSolutionVector>
    Scalar solidDensity(const Element &element,
                        const SubControlVolume& scv,
                        const ElementSolutionVector &elemSol) const
    {
        return 0;
    }

     /*!
     * \brief Returns the thermal conductivity \f$\mathrm{[W/(m K)]}\f$ of the porous material.
     *
     * \param globalPos The position of the center of the element
     *TODO this is a themporary fix because the implicit VolumeVariables need that.
     * We have a different non-isothermal localresidual so that this is never used because
     * we do not have heat storage in the matrix
     */
    template<class ElementSolutionVector>
    Scalar solidThermalConductivity(const Element &element,
                                    const SubControlVolume& scv,
                                    const ElementSolutionVector &elemSol) const
    {
        return 0;
    }

    const Problem& problem() const
    {
        return problem_;
    }

private:

     /*!
     * \brief Retrieves the vertex-related pore paramaters from the GridCreator. Can be overloaded for using special parameters
     *
     */
    void getVertexParamters_()
    {
        const auto numPores = gridView().size(dim);
        poreRadius_.resize(numPores);
        poreLabel_.resize(numPores);
        numAdjacentThroats_.resize(numPores);

        const bool isDgf = GridCreator::gridProperties().gridType() == GridCreator::GridType::dgf;

        const auto poreRadiusIdx = isDgf ? 0 : GridCreator::poreRadiusIdx;
        const auto poreLabelIdx = isDgf ? 1 : GridCreator::boundaryIdx;

        for(const auto& vertex : vertices(gridView()))
        {
            const int vIdx = gridView().indexSet().index(vertex);
            const auto& params = GridCreator::parameters(vertex);
            poreRadius_[vIdx] = params[poreRadiusIdx];
            assert(poreRadius_[vIdx] > 0.0);
            poreLabel_[vIdx] = params[poreLabelIdx];
        }

        numAdjacentThroats_ = GridCreator::computeCoordinationNumbers();

        // free some memory
        if(!isDgf)
            GridCreator::deleteVertexParamterContainers();
    }

     /*!
     * \brief Retrieves the element-related throat paramaters from the GridCreator. Can be overloaded for using special parameters
     *
     */
    void getElementParamters_()
    {
        const auto numThroats = gridView().size(0);
        throatRadius_.resize(numThroats);
        throatLength_.resize(numThroats);
        throatLabel_.resize(numThroats);

        const bool isDgf = GridCreator::gridProperties().gridType() == GridCreator::GridType::dgf;

        const auto throatRadiusIdx = isDgf ? 0 : GridCreator::throatRadiusIdx;
        const auto throatLengthIdx = isDgf ? 1 : GridCreator::throatLengthIdx;

        const auto throatLabel = [isDgf] (const auto& element)
        {
            if(isDgf && GridCreator::parameters(element).size() < 3) // DGF file does not specify throatLabel
                return GridCreator::throatLabel(element);
            else
                return static_cast<int>(GridCreator::parameters(element).back()); // the last parameter is the throatLabel
        };

        for(const auto& element : elements(gridView()))
        {
            const int eIdx = gridView().indexSet().index(element);
            const auto params = GridCreator::parameters(element);

            throatRadius_[eIdx] = params[throatRadiusIdx];
            throatLength_[eIdx] = params[throatLengthIdx];
            assert(throatRadius_[eIdx] > 0.0);
            assert(throatLength_[eIdx] > 0.0);
            throatLabel_[eIdx] = throatLabel(element);
        }

        // free some memory
        if(!isDgf)
            GridCreator::deleteElementParamterContainers();
    }

    const Problem& problem_;
    const GridView gridView_;
    bool useBoundaryDataFromSpatialParams_;

    const Scalar eps_;
    // TODO: use shared pointers!
    std::vector<Scalar> poreRadius_;
    std::vector<int> poreLabel_; // 0:no, 1:general, 2:coupling1, 3:coupling2, 4:inlet, 5:outlet
    std::vector<int> throatLabel_; // 0:no, 1:general, 2:coupling1, 3:coupling2, 4:inlet, 5:outlet
    std::vector<Scalar> throatRadius_;
    std::vector<Scalar> throatLength_;
    std::vector<unsigned int> numAdjacentThroats_;
    std::string boundaryDefinition_;
    std::vector<Scalar> factor_;

    Implementation &asImp_()
    {
        return *static_cast<Implementation*> (this);
    }

    const Implementation &asImp_() const
    {
        return *static_cast<const Implementation*> (this);
    }
};

} // namespace Dumux

#endif
