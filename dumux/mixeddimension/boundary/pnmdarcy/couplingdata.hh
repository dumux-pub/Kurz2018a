// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoubdaryCoupling
 * \ingroup BoxModel
 * \copydoc Dumux::PNMDarcyCouplingManager
 */

#ifndef DUMUX_PNM_DARCY_COUPLINGDATA_HH
#define DUMUX_PNM_DARCY_COUPLINGDATA_HH

#include <dumux/common/properties.hh>
#include <dumux/multidomain/couplingmanager.hh>
#include <dumux/discretization/evalsolution.hh>


namespace Dumux {

/*!
 * \ingroup MixedDimension
 * \ingroup MixedDimensionBoundary
 * \brief Coupling manager for low-dimensional domains coupled at the boundary to the bulk
 *        domain.
 */
template<class MDTraits, class CouplingManager>
class PNMDarcyCouplingData
{
    using Scalar = typename MDTraits::Scalar;
    static constexpr auto bulkIdx = typename MDTraits::template DomainIdx<0>();
    static constexpr auto lowDimIdx = typename MDTraits::template DomainIdx<1>();
    using SolutionVector = typename MDTraits::SolutionVector;

    // the sub domain type tags
    template<std::size_t id>
    using SubDomainTypeTag = typename MDTraits::template SubDomainTypeTag<id>;

    template<std::size_t id> using GridView = typename GET_PROP_TYPE(SubDomainTypeTag<id>, GridView);
    template<std::size_t id> using Problem = typename GET_PROP_TYPE(SubDomainTypeTag<id>, Problem);
    template<std::size_t id> using PointSource = typename GET_PROP_TYPE(SubDomainTypeTag<id>, PointSource);
    template<std::size_t id> using PrimaryVariables = typename GET_PROP_TYPE(SubDomainTypeTag<id>, PrimaryVariables);
    template<std::size_t id> using NumEqVector = typename GET_PROP_TYPE(SubDomainTypeTag<id>, NumEqVector);
    template<std::size_t id> using ElementSolutionVector = typename GET_PROP_TYPE(SubDomainTypeTag<id>, ElementSolutionVector);
    template<std::size_t id> using VolumeVariables = typename GET_PROP_TYPE(SubDomainTypeTag<id>, VolumeVariables);
    template<std::size_t id> using ElementVolumeVariables = typename GET_PROP_TYPE(SubDomainTypeTag<id>, ElementVolumeVariables);
    template<std::size_t id> using FVGridGeometry = typename GET_PROP_TYPE(SubDomainTypeTag<id>, FVGridGeometry);
    template<std::size_t id> using FVElementGeometry = typename FVGridGeometry<id>::LocalView;
    template<std::size_t id> using ElementBoundaryTypes = typename GET_PROP_TYPE(SubDomainTypeTag<id>, ElementBoundaryTypes);
    template<std::size_t id> using ElementFluxVariablesCache = typename GET_PROP_TYPE(SubDomainTypeTag<id>, ElementFluxVariablesCache);
    template<std::size_t id> using Element = typename GridView<id>::template Codim<0>::Entity;
    template<std::size_t id> using LocalResidual = typename GET_PROP_TYPE(SubDomainTypeTag<id>, LocalResidual);
    template<std::size_t id> using ElementResidualVector = typename LocalResidual<id>::ElementResidualVector;


public:
    PNMDarcyCouplingData(const CouplingManager& couplingmanager): couplingManager_(couplingmanager) {}

    NumEqVector<lowDimIdx> boundaryPointSource(std::size_t id, const bool verbose = false) const
    {
        const auto& context = couplingManager_.pointSourceContext(id);
        return getFlux(context.element, context.fvGeometry, context.elemVolVars, context.elemFluxVarsCache, verbose);
    }

     /*!
     * \brief Returns the cumulative flux in \f$\mathrm{[\frac{kg}{s}]}\f$ of several pore throats at a given location on the boundary
     *
     * \param problem The problem
     * \param element The element
     * \param considerScv A lambda function to decide whether to consider a scv or not
     * \param verbose If set true, the fluxes at all individual SCVs are printed
     */
    NumEqVector<lowDimIdx> getFlux(const Element<lowDimIdx>& element,
                                   const FVElementGeometry<lowDimIdx>& fvGeometry,
                                   const ElementVolumeVariables<lowDimIdx>& elemVolVars,
                                   const ElementFluxVariablesCache<lowDimIdx>& elemFluxVarsCache,
                                   const bool verbose = false) const
    {
        NumEqVector<lowDimIdx> flux(0.0);

        const auto& problem = couplingManager_.problem(lowDimIdx);

        // bool isStationary_ = true;
        LocalResidual<lowDimIdx> localResidual(&problem); //TODO instationary

        const auto residual = localResidual.computeBoundaryFlux(problem, element, fvGeometry, elemVolVars, elemFluxVarsCache);

        for(auto&& scv : scvs(fvGeometry))
        {
            // compute the boundary flux using the local residual of the element's scv on the boundary
            if(couplingManager_.isCoupledDof(lowDimIdx, scv.dofIndex()))
            {
                // The flux must be substracted:
                // On an inlet boundary, the flux part of the local residual will be positive, since all fluxes will leave the SCV towards to interior domain.
                // For the domain itself, however, the sign has to be negative, since mass is entering the system.
                flux -= residual[scv.indexInElement()];

                if(verbose)
                {
                    std::cout << "SCV of element " << scv.elementIndex()  << " at vertex " << scv.dofIndex() << " has flux: " << residual[scv.indexInElement()] << std::endl;
                }
            }
        }
        return flux;
    }

    PrimaryVariables<bulkIdx> bulkPrivarAtPos(const typename Element<bulkIdx>::Geometry::GlobalCoordinate& globalPos) const
    {
        const auto& context = couplingManager_.lowDimCouplingContext()[0];

        return evalSolution(context.element,
                            context.geometry,
                            couplingManager_.problem(bulkIdx).fvGridGeometry(),
                            context.elemSol,
                            globalPos);
    }

    // }
private:
    const CouplingManager& couplingManager_;

};

} // end namespace Dumux

#endif
