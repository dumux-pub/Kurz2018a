// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoubdaryCoupling
 * \ingroup BoxModel
 * \copydoc Dumux::PNMDarcyCouplingManager
 */

#ifndef DUMUX_PNM_DARCY_COUPLINGMANAGER_HH
#define DUMUX_PNM_DARCY_COUPLINGMANAGER_HH

#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <memory>

#include <dune/common/timer.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/referenceelements.hh>

#include <dumux/common/properties.hh>
#include <dumux/multidomain/couplingmanager.hh>

#include "couplingdata.hh"

namespace Dumux {

/*!
 * \ingroup MixedDimension
 * \ingroup MixedDimensionBoundary
 * \brief Coupling manager for low-dimensional domains coupled at the boundary to the bulk
 *        domain.
 */
template<class MDTraits>
class PNMDarcyCouplingManager
: public CouplingManager<MDTraits, PNMDarcyCouplingManager<MDTraits>>
{
    using Scalar = typename MDTraits::Scalar;

public:
    static constexpr auto bulkIdx = typename MDTraits::template DomainIdx<0>();
    static constexpr auto lowDimIdx = typename MDTraits::template DomainIdx<1>();
private:

    using SolutionVector = typename MDTraits::SolutionVector;



    // obtain the type tags of the sub problems
    using BulkTypeTag = typename MDTraits::template SubDomainTypeTag<0>;
    using LowDimTypeTag = typename MDTraits::template SubDomainTypeTag<1>;

    using LowDimToBulkMap = std::unordered_map<unsigned int, unsigned int>;

    using CouplingData = PNMDarcyCouplingData<MDTraits, PNMDarcyCouplingManager<MDTraits>>;

    using CouplingStencils = std::unordered_map<std::size_t, std::vector<std::size_t> >;
    using CouplingStencil = CouplingStencils::mapped_type;

    // the sub domain type tags
    template<std::size_t id>
    using SubDomainTypeTag = typename MDTraits::template SubDomainTypeTag<id>;

    template<std::size_t id>
    using SubDomainSolutionVector = typename GET_PROP_TYPE(SubDomainTypeTag<id>, SolutionVector);

    template<std::size_t id> using GridView = typename GET_PROP_TYPE(SubDomainTypeTag<id>, GridView);
    template<std::size_t id> using Problem = typename GET_PROP_TYPE(SubDomainTypeTag<id>, Problem);
    template<std::size_t id> using PointSource = typename GET_PROP_TYPE(SubDomainTypeTag<id>, PointSource);
    template<std::size_t id> using PrimaryVariables = typename GET_PROP_TYPE(SubDomainTypeTag<id>, PrimaryVariables);
    template<std::size_t id> using NumEqVector = typename GET_PROP_TYPE(SubDomainTypeTag<id>, NumEqVector);
    template<std::size_t id> using ElementSolutionVector = typename GET_PROP_TYPE(SubDomainTypeTag<id>, ElementSolutionVector);
    template<std::size_t id> using VolumeVariables = typename GET_PROP_TYPE(SubDomainTypeTag<id>, VolumeVariables);
    template<std::size_t id> using ElementVolumeVariables = typename GET_PROP_TYPE(SubDomainTypeTag<id>, ElementVolumeVariables);
    template<std::size_t id> using FVGridGeometry = typename GET_PROP_TYPE(SubDomainTypeTag<id>, FVGridGeometry);
    template<std::size_t id> using FVElementGeometry = typename FVGridGeometry<id>::LocalView;
    template<std::size_t id> using ElementBoundaryTypes = typename GET_PROP_TYPE(SubDomainTypeTag<id>, ElementBoundaryTypes);
    template<std::size_t id> using ElementFluxVariablesCache = typename GET_PROP_TYPE(SubDomainTypeTag<id>, ElementFluxVariablesCache);
    template<std::size_t id> using Element = typename GridView<id>::template Codim<0>::Entity;
    template<std::size_t id> using LocalResidual = typename GET_PROP_TYPE(SubDomainTypeTag<id>, LocalResidual);
    template<std::size_t id> using ElementResidualVector = typename LocalResidual<id>::ElementResidualVector;

    static constexpr auto lowDimDim = GridView<lowDimIdx>::dimension;
    static constexpr auto dimWorld = GridView<bulkIdx>::dimensionworld;

    struct StationaryBulkCouplingContext
    {
        Element<lowDimIdx> element;
        FVElementGeometry<lowDimIdx> fvGeometry;
        ElementVolumeVariables<lowDimIdx> elemVolVars;
        ElementFluxVariablesCache<lowDimIdx> elemFluxVarsCache;
    };

    struct StationaryLowDimCouplingContext
    {
        Element<bulkIdx> element;
        typename Element<bulkIdx>::Geometry geometry;
        ElementSolutionVector<bulkIdx>  elemSol;
    };

public:

    /*!
     * \brief Constructor
     */
    PNMDarcyCouplingManager(std::shared_ptr<const FVGridGeometry<bulkIdx>> bulkFvGridGeometry,
                            std::shared_ptr<const FVGridGeometry<lowDimIdx>> lowDimFvGridGeometry)
    {

        // Check if we are using the cellcentered method in both domains
        static_assert(lowDimDim == 1, "The bounding box coupling manager only works with one-dimensional low-dim grids");

        computeCouplingMaps(*bulkFvGridGeometry, *lowDimFvGridGeometry);
    }

    /*!
     * \brief Methods to be accessed by main
     */
    // \{

    void init(std::shared_ptr<Problem<bulkIdx>> bulkProblem,
              std::shared_ptr<Problem<lowDimIdx>> lowDimProblem,
              const SolutionVector& curSol)
    {
        curSol_ = curSol;
        problemTuple_ = std::make_tuple(bulkProblem, lowDimProblem);
        couplingData_ = std::make_shared<CouplingData>(*this);
    }

    /*!
     * \brief Update after the grid has changed
     */
    void update()
    {

    }

    /*!
     * \brief Update the solution vector before assembly
     */
    void updateSolution(const SolutionVector& curSol)
    { curSol_ = curSol; }

    /*!
     * \brief Update the solution vector before assembly
     */
    template<std::size_t id>
    void updateSolution(const SubDomainSolutionVector<id>& curSol, Dune::index_constant<id> domainId)
    { curSol_[domainId] = curSol; }

    // \}

    /*!
     * \brief Methods to be accessed by the assembly
     */
    // \{

    /*!
     * \brief The bulk coupling stencil, i.e. which low-dimensional dofs
     *        the given bulk element dof depends on.
     */
    const std::vector<std::size_t>& couplingStencil(const Element<bulkIdx>& element, Dune::index_constant<1> lowDimIdx) const
    {
        const std::size_t eIdx = problem(bulkIdx).fvGridGeometry().elementMapper().index(element);
        if (bulkCouplingStencils_.count(eIdx))
            return bulkCouplingStencils_.at(eIdx);
        else
            return emptyStencil_;
    }

    /*!
     * \brief The low dim coupling stencil, i.e. which bulk dofs
     *        the given low dimensional element dof depends on.
     */
    const std::vector<std::size_t>& couplingStencil(const Element<lowDimIdx>& element, Dune::index_constant<0> bulkIdx) const
    {
        const std::size_t eIdx = problem(lowDimIdx).fvGridGeometry().elementMapper().index(element);
        if (lowDimCouplingStencils_.count(eIdx))
            return lowDimCouplingStencils_.at(eIdx);
        else
            return emptyStencil_;
    }

    //! evaluate coupling residual for the derivative bulk DOF with respect to low dim DOF
    //! we only need to evaluate the part of the residual that will be influence by the low dim DOF
    template<class LocalAssembler>
    ElementResidualVector<bulkIdx> evalCouplingResidual(const Element<bulkIdx>& element, const LocalAssembler& assembler)
    {
        return assembler.evalLocalResidual();
    }

    //! evaluate coupling residual for the derivative low dim DOF with respect to bulk DOF
    //! we only need to evaluate the part of the residual that will be influence by the bulk DOF
    template<class LocalAssembler>
    ElementResidualVector<lowDimIdx> evalCouplingResidual(const Element<lowDimIdx>& element, const LocalAssembler& assembler)
    {
        ElementResidualVector<lowDimIdx> partialDerivs(element.subEntities(GridView<lowDimIdx>::dimension));
        partialDerivs = 0.0;

        auto fvGeometry = localView(problem(lowDimIdx).fvGridGeometry());
        fvGeometry.bindElement(element);

        for(const auto& scv : scvs(fvGeometry))
        {
            if(isCoupledDof(lowDimIdx, scv.dofIndex()))
                partialDerivs[scv.indexInElement()] = curSol_[lowDimIdx][scv.dofIndex()] - couplingData().bulkPrivarAtPos(scv.dofPosition());
        }

        return partialDerivs;
    }

    /*!
     * \brief Bind the coupling context
     */
    template<std::size_t i, class Assembler>
    void bindCouplingContext(Dune::index_constant<i> domainI, const Element<bulkIdx>& element, const Assembler& assembler)
    {
        bulkCouplingContext_.clear();

        const auto bulkElementIdx  = problem(bulkIdx).fvGridGeometry().elementMapper().index(element);

        // do nothing if the element is not coupled to the other domain
        if(!bulkCouplingStencils_.count(bulkElementIdx))
            return;

        auto fvGeometry = localView(problem(lowDimIdx).fvGridGeometry());
        auto elemVolVars = localView(assembler.gridVariables(lowDimIdx).curGridVolVars());
        auto elemFluxVarsCache = localView(assembler.gridVariables(lowDimIdx).gridFluxVarsCache());

        // count the number of point sources (i.e. lowDim coupling contexts)
        // affiliated to the bulk element
        int counter = 0;
        pointSourceToContextMap_.clear();

        assert(bulkElementToPointSourcesMap_.count(bulkElementIdx));
        for(const auto& sourceId : bulkElementToPointSourcesMap_.at(bulkElementIdx))
        {
            // prepare the context of the point source
            const auto adjacentElementIndex = neighboringLowDimElements_[sourceId];
            const auto& adjacentElement = problem(lowDimIdx).fvGridGeometry().boundingBoxTree().entitySet().entity(adjacentElementIndex);

            fvGeometry.bindElement(adjacentElement);
            elemVolVars.bind(adjacentElement, fvGeometry, curSol_[lowDimIdx]);
            elemFluxVarsCache.bind(adjacentElement, fvGeometry, elemVolVars);

            // add the coupling context
            bulkCouplingContext_.push_back({adjacentElement, fvGeometry, elemVolVars, elemFluxVarsCache});

            // make the point source's context uniquely identifiable
            pointSourceToContextMap_[sourceId] = counter++;
        }
    }

    /*!
     * \brief Bind the coupling context
     */
    template<std::size_t i, class Assembler>
    void bindCouplingContext(Dune::index_constant<i> domainI, const Element<lowDimIdx>& element, const Assembler& assembler)
    {
        lowDimCouplingContext_.clear();

        const auto lowDimElementIdx = problem(lowDimIdx).fvGridGeometry().elementMapper().index(element);

        // do nothing if the element is not coupled to the other domain
        if(!lowDimCouplingStencils_.count(lowDimElementIdx))
            return;

        assert(lowDimElementToBulkElementMap_.count(lowDimElementIdx));
        const auto bulkElementIdx = lowDimElementToBulkElementMap_.at(lowDimElementIdx);

        // prepare the coupling context
        const auto& bulkElement = problem(bulkIdx).fvGridGeometry().boundingBoxTree().entitySet().entity(bulkElementIdx);
        auto fvGeometry = localView(problem(bulkIdx).fvGridGeometry());
        fvGeometry.bindElement(bulkElement);
        ElementSolutionVector<bulkIdx> elemSol(bulkElement, curSol_[bulkIdx], fvGeometry);

        // add the context
        lowDimCouplingContext_.push_back({bulkElement, bulkElement.geometry(), elemSol});
    }

    /*!
     * \brief Update the coupling context for a derivative i->j
     */
    template<std::size_t i, std::size_t j, class Assembler>
    void updateCouplingContext(Dune::index_constant<i> domainI,
                               Dune::index_constant<j> domainJ,
                               const Element<j>& element,
                               const PrimaryVariables<j>& priVars,
                               const Assembler& assembler)
    {
        // const auto eIdx = problem(domainJ).fvGridGeometry().elementMapper().index(element);
        // curSol_[domainJ][eIdx] = priVars;
    }

    /*!
     * \brief Update the coupling context for a derivative i->j
     */
    template<std::size_t i, std::size_t j, class Assembler>
    void updateCouplingContext(Dune::index_constant<i> domainI,
                               Dune::index_constant<j> domainJ,
                               const std::size_t dofIndex,
                               const PrimaryVariables<j>& priVars,
                               const Assembler& assembler)
    {
        curSol_[domainJ][dofIndex] = priVars;
        updateCouplingContext(domainJ);
    }

    void updateCouplingContext(Dune::index_constant<1>)
    {
        for(auto& lowDimData : bulkCouplingContext_)
        {
            ElementSolutionVector<lowDimIdx> elemSol(lowDimData.element, curSol_[lowDimIdx], lowDimData.fvGeometry);

            for(const auto& scv : scvs(lowDimData.fvGeometry))
            {
                lowDimData.elemVolVars[scv].update(elemSol, problem(lowDimIdx), lowDimData.element, scv);
            }
        }
    }

    void updateCouplingContext(Dune::index_constant<0>)
    {
        for(auto& bulkData : lowDimCouplingContext_)
        {
            auto fvGeometry = localView(problem(bulkIdx).fvGridGeometry());
            fvGeometry.bind(bulkData.element);
            ElementSolutionVector<bulkIdx> elemSol(bulkData.element, curSol_[bulkIdx], fvGeometry);
            bulkData.elemSol = elemSol;
        }
    }

    // \}

    /*!
     * \brief Main update routine
     */
    // \{
    void computeCouplingMaps(const FVGridGeometry<bulkIdx>& bulkFvGridGeometry,
                             const FVGridGeometry<lowDimIdx>& lowDimFvGridGeometry)
    {
        std::size_t pointSourceId = 0;
        isCoupledLowDimDof_.resize(lowDimFvGridGeometry.numDofs(), false);

        auto lowDimFvGeometry = localView(lowDimFvGridGeometry);
        auto bulkFvGeometry = localView(bulkFvGridGeometry);
        const auto& lowDimGridView = lowDimFvGridGeometry.gridView();

        // iterate over the lowDim elements
        for(const auto& lowDimElement : elements(lowDimGridView))
        {
            // check if the lowDim vertices intersect with the bulk grid
            lowDimFvGeometry.bindElement(lowDimElement);
            for(const auto& lowDimScv : scvs(lowDimFvGeometry))
            {
                const auto lowDimPos = lowDimScv.dofPosition();
                auto bulkElementIndices = intersectingEntities(lowDimPos, bulkFvGridGeometry.boundingBoxTree());
                if(bulkElementIndices.empty())
                    continue;

                const auto lowDimDofIdx = lowDimScv.dofIndex();
                const auto& otherLowDimScv = lowDimFvGeometry.scv(1 - lowDimScv.indexInElement());
                const auto otherLowDimScvDofIdx = otherLowDimScv.dofIndex();
                const auto lowDimElementIdx = lowDimFvGridGeometry.elementMapper().index(lowDimElement);

                neighboringLowDimElements_.push_back(lowDimElementIdx);
                isCoupledLowDimDof_[lowDimDofIdx] = true;

                // if there are intersections, fill the coupling stencils
                for(const auto& bulkEIdx : bulkElementIndices)
                {
                    bulkCouplingStencils_[bulkEIdx].push_back(lowDimDofIdx);
                    bulkCouplingStencils_[bulkEIdx].push_back(otherLowDimScvDofIdx);

                    bulkElementToPointSourcesMap_[bulkEIdx].push_back(pointSourceId);

                    const auto& bulkElement = bulkFvGridGeometry.boundingBoxTree().entitySet().entity(bulkEIdx);

                    bulkFvGeometry.bindElement(bulkElement);
                    for(const auto& bulkScv : scvs(bulkFvGeometry))
                        lowDimCouplingStencils_[lowDimElementIdx].push_back(bulkScv.dofIndex());
                }

                lowDimElementToBulkElementMap_[lowDimElementIdx] = bulkElementIndices[0];

                bulkPointSources_.emplace_back(lowDimPos, pointSourceId);
                ++pointSourceId;
            }

        }

        // remove duplicate entries from coupling stencils
        for(auto&& stencil : bulkCouplingStencils_)
            removeDuplicates_(stencil.second);
        for(auto&& stencil : lowDimCouplingStencils_)
            removeDuplicates_(stencil.second);

    }

    //! Return a reference to the point sources
    const std::vector<PointSource<bulkIdx>>& bulkPointSources() const
    {
        return bulkPointSources_;
    }

    const auto& couplingData() const
    {
        return *couplingData_;
    }

    const auto& bulkCouplingContext() const
    {
        return bulkCouplingContext_;
    }

    const auto& lowDimCouplingContext() const
    {
        return lowDimCouplingContext_;
    }

    const auto& pointSourceContext(std::size_t id) const
    {
        assert(pointSourceToContextMap_.count(id));
        return bulkCouplingContext_[pointSourceToContextMap_.at(id)];
    }

    // \}

    //! Return reference to bulk coupling stencil member
    template<std::size_t id>
    const CouplingStencils& couplingStencils(Dune::index_constant<id> dom) const
    { return (id == bulkIdx) ? bulkCouplingStencils_ : lowDimCouplingStencils_; }

    /*!
     * \brief The coupling stencil of domain I, i.e. which domain J dofs
     *        the given domain I element's residual depends on.
     */
    template<std::size_t i, std::size_t j>
    const CouplingStencil& couplingStencil(const Element<i>& element,
                                           Dune::index_constant<i> domainI,
                                           Dune::index_constant<j> domainJ) const
    {
        static_assert(i != j, "A domain cannot be coupled to itself!");

        const auto eIdx = problem(domainI).fvGridGeometry().elementMapper().index(element);
        if (couplingStencils(domainI).count(eIdx))
            return couplingStencils(domainI).at(eIdx);
        else
            return emptyStencil_;
    }

    //! Clear all internal data members
    void clear()
    {
        // bulkPointSources_.clear();
        // lowDimPointSources_.clear();
        // pointSourceData_.clear();
        // bulkCouplingStencils_.clear();
        // lowDimCouplingStencils_.clear();
        // idCounter_ = 0;
    }

    //! Return a reference to the bulk problem
    template<std::size_t id>
    const Problem<id>& problem(Dune::index_constant<id> domainIdx) const
    {
        return *std::get<id>(problemTuple_);
    }

    //! Return a reference to the bulk problem
    template<std::size_t id>
    Problem<id>& problem(Dune::index_constant<id> domainIdx)
    {
        return *std::get<id>(problemTuple_);
    }

    template<class IdType>
    const std::vector<std::size_t>& getAdditionalDofDependencies(IdType id, std::size_t bulkElementIdx) const
    { return emptyStencil_; }

    bool isCoupledDof(Dune::index_constant<0>, std::size_t bulkDofIdx) const
    {
        DUNE_THROW(Dune::NotImplemented, "isCoupledDof for bulkDofs");
        return false;
    }

    bool isCoupledDof(Dune::index_constant<1>, std::size_t lowDimDofIdx) const
    {
        return isCoupledLowDimDof_[lowDimDofIdx];
    }

    const auto& curSol() const
    {
        return curSol_;
    }


protected:

    //! Return reference to bulk coupling stencil member
    std::unordered_map<std::size_t, std::vector<std::size_t> >& bulkCouplingStencils()
    { return bulkCouplingStencils_; }

    //! Return reference to low dim coupling stencil member
    std::unordered_map<std::size_t, std::vector<std::size_t> >& lowDimCouplingStencils()
    { return lowDimCouplingStencils_; }

    //! Return a reference to an empty stencil
    std::vector<std::size_t>& emptyStencil()
    { return emptyStencil_; }

    void removeDuplicates_(std::vector<std::size_t>& stencil)
    {
        std::sort(stencil.begin(), stencil.end());
        stencil.erase(std::unique(stencil.begin(), stencil.end()), stencil.end());
    }

private:

    std::tuple<std::shared_ptr<Problem<0>>, std::shared_ptr<Problem<1>>> problemTuple_;

    LowDimToBulkMap lowDimElementToBulkElementMap_;

    std::vector<bool> isCoupledLowDimDof_;

    std::unordered_map<std::size_t, std::size_t> pointSourceToContextMap_; // TODO: maybe better use a vector?

    std::unordered_map<std::size_t, std::vector<std::size_t> > bulkElementToPointSourcesMap_;

    std::shared_ptr<CouplingData> couplingData_;

    std::vector<std::size_t> neighboringLowDimElements_;


    std::unordered_map<std::size_t, std::vector<std::size_t> > bulkCouplingStencils_;
    std::unordered_map<std::size_t, std::vector<std::size_t> > lowDimCouplingStencils_;
    std::vector<std::size_t> emptyStencil_;

    std::vector<PointSource<bulkIdx>> bulkPointSources_;

    ////////////////////////////////////////////////////////////////////////////
    //! The coupling context
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //! TODO: this is the simplest context -> just the solutionvector
    ////////////////////////////////////////////////////////////////////////////
    SolutionVector curSol_;

    std::vector<StationaryBulkCouplingContext> bulkCouplingContext_;
    std::vector<StationaryLowDimCouplingContext> lowDimCouplingContext_;
};

} // end namespace Dumux

#endif
