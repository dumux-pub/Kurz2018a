// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup PoreNetworkOnePModel
 * \copydoc Dumux::PNMOnePVtkOutputFields
 */
#ifndef DUMUX_PNM_ONEP_VTK_OUTPUT_FIELDS_HH
#define DUMUX_PNM_ONEP_VTK_OUTPUT_FIELDS_HH

#include <dumux/porenetworkflow/common/vtkoutputfields.hh>
#include <dumux/porousmediumflow/1p/vtkoutputfields.hh>

namespace Dumux
{

/*!
 * \ingroup PoreNetworkOnePNCModel
 * \brief Adds vtk output fields specific to the PNM 1pnc model
 */
template<int phaseIdx>
class PNMOnePVtkOutputFields
{
public:
    template <class VtkOutputModule>
    static void init(VtkOutputModule& vtk)
    {
        OnePVtkOutputFields::init(vtk);
        PNMCommonVtkOutputFields::init(vtk);

        vtk.addThroatFluxVariable([](const auto& problem, const auto& fluxVars, const auto& fluxVarsCache)
                             { return fluxVarsCache.transmissibility(phaseIdx); }, "transmissibility");

        auto volumeFlux = [](const auto& problem, const auto& fluxVars, const auto& fluxVarsCache)
        {
            auto upwindTerm = [](const auto& volVars) { return volVars.mobility(phaseIdx); };
            using std::abs;
            return abs(fluxVars.advectiveFlux(phaseIdx, upwindTerm));
        };
        vtk.addThroatFluxVariable(volumeFlux, "volumeFlux");
    }
};

} // end namespace Dumux

#endif
