// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief  Adaption of the fully implicit scheme to the one-phase n-component pore network model.
 */

#ifndef DUMUX_PNM1PNC_MODEL_HH
#define DUMUX_PNM1PNC_MODEL_HH

#include <dumux/common/properties.hh>
#include <dumux/porenetworkflow/properties.hh>

#include <dumux/porenetworkflow/1p/model.hh>

#include <dumux/porousmediumflow/compositional/localresidual.hh>
#include <dumux/porousmediumflow/nonisothermal/model.hh>

#include <dumux/porousmediumflow/1pnc/model.hh>
#include <dumux/porousmediumflow/1pnc/vtkoutputfields.hh>
#include <dumux/material/fluidmatrixinteractions/diffusivitymillingtonquirk.hh>

#include <dumux/material/spatialparams/porenetwork/porenetwork1p.hh>
#include <dumux/material/fluidmatrixinteractions/porenetwork/transmissibility.hh>

#include <dumux/material/fluidsystems/gasphase.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>
#include <dumux/material/fluidstates/immiscible.hh>

#include "vtkoutputfields.hh"

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tags for the implicit single-phase mulit-component problems
NEW_TYPE_TAG(PNMOnePNC, INHERITS_FROM(PNMOneP));

//! The type tags for the corresponding non-isothermal problems
NEW_TYPE_TAG(PNMOnePNCNI, INHERITS_FROM(PNMOnePNC, NonIsothermal));

///////////////////////////////////////////////////////////////////////////
// properties for the isothermal single phase model
///////////////////////////////////////////////////////////////////////////

/*!
 * \brief Set the property for the number of components.
 *
 * We just forward the number from the fluid system
 *
 */
SET_PROP(PNMOnePNC, NumComponents)
{
private:
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem));

public:
    static constexpr auto value = FluidSystem::numComponents;
};

/*!
 * \brief Set the property for the number of equations: For each existing component one equation has to be solved.
 */
SET_PROP(PNMOnePNC, NumEq)
{
private:
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem));
public:
    static constexpr auto value = FluidSystem::numComponents;
};

//! Set as default that no component mass balance is replaced by the total mass balance
SET_PROP(PNMOnePNC, ReplaceCompEqIdx)
{
private:
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem));
public:
    static constexpr auto value = FluidSystem::numComponents;
};

/*!
 * \brief The fluid state which is used by the volume variables to
 *        store the thermodynamic state. This should be chosen
 *        appropriately for the model ((non-)isothermal, equilibrium, ...).
 *        This can be done in the problem.
 */
SET_PROP(PNMOnePNC, FluidState){
    private:
        using Scalar =  typename GET_PROP_TYPE(TypeTag, Scalar);
        using FluidSystem =  typename GET_PROP_TYPE(TypeTag, FluidSystem);
    public:
        using type = Dumux::CompositionalFluidState<Scalar, FluidSystem>;
};

//! Use the model after Millington (1961) for the effective diffusivity
SET_TYPE_PROP(PNMOnePNC, EffectiveDiffusivityModel,
             DiffusivityMillingtonQuirk<typename GET_PROP_TYPE(TypeTag, Scalar)>);

//! The local residual function
SET_TYPE_PROP(PNMOnePNC, LocalResidual, PNMLocalResidual<TypeTag, CompositionalLocalResidual<TypeTag> >);


SET_INT_PROP(PNMOnePNC, NumPhases, 1); //!< The number of phases in the 1pnc model is 1
SET_INT_PROP(PNMOnePNC, PhaseIdx, 0); //!< The default phase index
SET_TYPE_PROP(PNMOnePNC, VolumeVariables, OnePNCVolumeVariables<TypeTag>);   //!< the VolumeVariables property

//!The indices required by the isothermal single-phase model
SET_TYPE_PROP(PNMOnePNC, Indices, OnePNCIndices <TypeTag, /*PVOffset=*/0>);


SET_BOOL_PROP(PNMOnePNC, EnableAdvection, true);                           //!< The one-phase model considers advection
SET_BOOL_PROP(PNMOnePNC, EnableMolecularDiffusion, true);                 //!< The one-phase model has no molecular diffusion
SET_BOOL_PROP(PNMOnePNC, EnableEnergyBalance, false);                      //!< Isothermal model by default

//!< Set the vtk output fields specific to this model
SET_PROP(PNMOnePNC, VtkOutputFields)
{
private:
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    static constexpr auto phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx);
public:
    using type = PNMOnePNCVtkOutputFields<FluidSystem, phaseIdx>;
};

SET_BOOL_PROP(PNMOnePNC, UseMoles, true);

//////////////////////////////////////////////////////////////////
// Property values for isothermal model required for the general non-isothermal model
//////////////////////////////////////////////////////////////////

SET_PROP(PNMOnePNCNI, IsothermalVtkOutputFields)
{
private:
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    static constexpr auto phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx);
public:
    using type = PNMOnePNCVtkOutputFields<FluidSystem, phaseIdx>;
};

//set isothermal LocalResidual
SET_TYPE_PROP(PNMOnePNCNI, IsothermalLocalResidual, PNMLocalResidual<TypeTag, CompositionalLocalResidual<TypeTag>>);

//set isothermal Indices
SET_TYPE_PROP(PNMOnePNCNI, IsothermalIndices, OnePNCIndices <TypeTag, /*PVOffset=*/0>);

/*!
 * \brief Set the property for the number of equations: For each existing component one equation has to be solved.
 */
SET_PROP(PNMOnePNCNI, IsothermalNumEq)
{
private:
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem));
public:
    static constexpr auto value = FluidSystem::numComponents;
};

SET_TYPE_PROP(PNMOnePNCNI,
              ThermalConductivityModel,
              ThermalConductivityAverage<typename GET_PROP_TYPE(TypeTag, Scalar)>); //!< Use the average for effective conductivities
}

}

#endif
