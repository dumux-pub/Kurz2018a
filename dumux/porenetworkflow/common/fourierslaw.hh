// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains the data which is required to calculate
 *        diffusive heat fluxes with Fourier's law.
 */
#ifndef DUMUX_PNM_FOURIERS_LAW_HH
#define DUMUX_PNM_FOURIERS_LAW_HH

#include <dumux/common/properties.hh>

namespace Dumux
{

/*!
 * \brief Specialization of Fourier's Law for the pore-network model.
 */
template <class TypeTag>
class PNMFouriersLaw
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using ElementFluxVariablesCache = typename GET_PROP_TYPE(TypeTag, ElementFluxVariablesCache);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);

    using Element = typename GridView::template Codim<0>::Entity;

    enum { numPhases = GET_PROP_VALUE(TypeTag, NumPhases)} ;

public:

    static Scalar flux(const Problem& problem,
                       const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolumeFace& scvf,
                       const ElementFluxVariablesCache& elemFluxVarsCache)
    {
        const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
        const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());
        const auto& insideVolVars = elemVolVars[insideScv];
        const auto& outsideVolVars = elemVolVars[outsideScv];
        Scalar heatflux = 0;
        for (int phaseIdx = 0; phaseIdx < numPhases; phaseIdx++)
        {
            auto insideThermalConducitivity =  insideVolVars.fluidThermalConductivity(phaseIdx);
            auto outsideThermalConducitivity =  outsideVolVars.fluidThermalConductivity(phaseIdx);

            auto thermalConductivity = problem.spatialParams().harmonicMean(insideThermalConducitivity, outsideThermalConducitivity, scvf.unitOuterNormal());
            auto area = problem.spatialParams().throatCrossSection(element, elemVolVars, phaseIdx);

            // calculate the temperature gradient
            const Scalar deltaT = insideVolVars.temperature() - outsideVolVars.temperature();
            const Scalar gradT = deltaT/ problem.spatialParams().throatLength(element, elemVolVars);

            heatflux += thermalConductivity*gradT*area;
        }
        return heatflux;
    }

};
} // end namespace

#endif
