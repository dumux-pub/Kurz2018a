// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the two-phase fully implicit pore network model.
 */
#ifndef DUMUX_PNM_BASE_LOCAL_RESIDUAL_HH
#define DUMUX_PNM_BASE_LOCAL_RESIDUAL_HH

#include <dumux/porenetworkflow/properties.hh>

namespace Dumux
{
/*!
 * \ingroup Pore network model
 * \ingroup ImplicitLocalResidual
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the one-phase fully implicit model.
 *
 * This class is also used for the non-isothermal model, which means
 * that it uses static polymorphism.
 */


template<class TypeTag, class ParentType>
class PNMLocalResidual :  public ParentType
{
protected:
    using Implementation = typename GET_PROP_TYPE(TypeTag, LocalResidual);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using FluxVariables = typename GET_PROP_TYPE(TypeTag, FluxVariables);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using ElementBoundaryTypes = typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes);
    using ElementFluxVariablesCache = typename GET_PROP_TYPE(TypeTag, ElementFluxVariablesCache);
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);
    using ResidualVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);


    enum
    {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
        dofCodim = dim,

        pressureIdx = Indices::pressureIdx,
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numEq = GET_PROP_VALUE(TypeTag, NumEq)
    };

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;

public:
    using ParentType::ParentType;
    using ElementResidualVector = typename ParentType::ElementResidualVector;

    /*!
     * \brief Calculate the source term of the equation
     *
     * \param scv The sub-control volume over which we integrate the source term
     *
     */
    ResidualVector computeSource(const Problem& problem,
                                   const Element& element,
                                   const FVElementGeometry& fvGeometry,
                                   const ElementVolumeVariables& elemVolVars,
                                   const SubControlVolume &scv) const
    {
        ResidualVector source(0);

        // add contributions from volume flux sources
        source += problem.source(element.template subEntity<dim>(scv.indexInElement()), elemVolVars[scv]);

        // add contribution from possible point sources
        source += problem.scvPointSources(element, fvGeometry, elemVolVars, scv);

        correctVolumeTerm_(problem, source, scv, elemVolVars[scv]);
        return source;
    }

    /*!
     * \brief Evaluate the rate of change of all conservation
     *        quantites (e.g. phase mass) within a sub-control
     *        volume of a finite volume element for the immiscible models.
     * \param scv The sub control volume
     * \param volVars The current or previous volVars
     * \note This function should not include the source and sink terms.
     * \note The volVars can be different to allow computing
     *       the implicit euler time derivative here
     */
    ResidualVector computeStorage(const Problem& problem,
                                    const SubControlVolume& scv,
                                    const VolumeVariables& volVars) const
    {
        ResidualVector storage = ParentType::computeStorage(problem, scv, volVars);
        correctVolumeTerm_(problem, storage, scv, volVars);
        return storage;
    }

    /*!
     * \brief Evaluates the integral flux leaving or entering the domain at a given element
     *        by partially evaluating the local residual
     *
     * \param problem The problem
     * \param element The DUNE Codim<0> entity for which the partial residual
     *                ought to be calculated
     * \param fvGeometry The fvGeometry
     * \param prevElemVolVars The element volume variables of the previous timestep
     * \param curElemVolVars The element volume variables of the current timestep
     * \param elemFluxVarsCache The element flux variables cache
     *
     */
    ElementResidualVector computeBoundaryFlux(const Problem& problem,
                                              const Element& element,
                                              const FVElementGeometry& fvGeometry,
                                              const ElementVolumeVariables& prevElemVolVars,
                                              const ElementVolumeVariables& curElemVolVars,
                                              const ElementFluxVariablesCache& elemFluxVarsCache) const
    {
        ElementResidualVector result(fvGeometry.numScv());
        result = 0.0;

        const auto& scvf = fvGeometry.scvf(0);

        const auto flux = this->asImp().computeFlux(problem, element, fvGeometry, curElemVolVars, scvf, elemFluxVarsCache);
        const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
        const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());
        result[insideScv.indexInElement()] += flux;
        result[outsideScv.indexInElement()] -= flux;

        for(auto&& scv : scvs(fvGeometry))
        {
            this->asImp().evalSource(result, problem, element, fvGeometry, curElemVolVars, scv);
            this->asImp().evalStorage(result, problem, element, fvGeometry, prevElemVolVars, curElemVolVars, scv);
        }

        return result;
    }

        /*!
     * \brief Evaluates the integral flux leaving or entering the domain at a given element
     *        by partially evaluating the local residual
     *
     * \param problem The problem
     * \param element The DUNE Codim<0> entity for which the partial residual
     *                ought to be calculated
     * \param fvGeometry The fvGeometry
     * \param prevElemVolVars The element volume variables of the previous timestep
     * \param curElemVolVars The element volume variables of the current timestep
     * \param elemFluxVarsCache The element flux variables cache
     *
     */
    ElementResidualVector computeBoundaryFlux(const Problem& problem,
                                              const Element& element,
                                              const FVElementGeometry& fvGeometry,
                                              const ElementVolumeVariables& curElemVolVars,
                                              const ElementFluxVariablesCache& elemFluxVarsCache) const
    {
        ElementResidualVector result(fvGeometry.numScv());
        result = 0.0;

        const auto& scvf = fvGeometry.scvf(0);

        const auto flux = this->asImp().computeFlux(problem, element, fvGeometry, curElemVolVars, scvf, elemFluxVarsCache);
        const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
        const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());
        result[insideScv.indexInElement()] += flux;
        result[outsideScv.indexInElement()] -= flux;

        for(auto&& scv : scvs(fvGeometry))
            this->asImp().evalSource(result, problem, element, fvGeometry, curElemVolVars, scv);

        return result;
    }

    void evalFlux(ElementResidualVector& residual,
                  const Problem& problem,
                  const Element& element,
                  const FVElementGeometry& fvGeometry,
                  const ElementVolumeVariables& elemVolVars,
                  const ElementBoundaryTypes& elemBcTypes,
                  const ElementFluxVariablesCache& elemFluxVarsCache,
                  const SubControlVolumeFace& scvf) const
    {
        if(scvf.index() != 0)
            return;

        const auto flux = evalFlux(problem, element, fvGeometry, elemVolVars, elemBcTypes, elemFluxVarsCache, scvf);

        const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
        const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());
        residual[insideScv.indexInElement()] += flux;
        residual[outsideScv.indexInElement()] -= flux;

    }


    ResidualVector evalFlux(const Problem& problem,
                            const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const ElementBoundaryTypes& elemBcTypes,
                            const ElementFluxVariablesCache& elemFluxVarsCache,
                            const SubControlVolumeFace& scvf) const
    {
        if(scvf.index() != 0)
            return ResidualVector(0.0);
        else
            return this->asImp().computeFlux(problem, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
    }



protected:

//TODO check if that is still necessary with next and if yes adapt it!
//     /*!
//      * \brief Add all Neumann and outflow boundary conditions to the local
//      *        residual.
//      */
//     void evalBoundaryFluxes_()
//     {
//         for (int scvIdx = 0; scvIdx < this->fvGeometry_().numScv; ++scvIdx) {
//             const auto &bcTypes = this->bcTypes_(scvIdx);
//
//             if (bcTypes.hasNeumann())
//                 evalNeumannDof_(scvIdx);
//
//             if(bcTypes.hasOutflow())
//                 evalOutFlowDof_(scvIdx);
//         }
//     }

//     void evalNeumannDof_(const int scvIdx)
//     {
//         const auto &bcTypes = this->bcTypes_(scvIdx);
//         // temporary vector to store the neumann boundary fluxes
//         PrimaryVariables neumannFlux(0.0);
//         this->problem().solDependentNeumann(neumannFlux,
//                                              this->element_().template subEntity<dim>(scvIdx),
//                                              this->curVolVars_()[scvIdx]);
//         Valgrind::CheckDefined(neumannFlux);
//
//         // set the neumann conditions
//         for (int eqIdx = 0; eqIdx < numEq; ++eqIdx) {
//             if (!bcTypes.isNeumann(eqIdx))
//                 continue;
//             this->residual_[scvIdx][eqIdx] += neumannFlux[eqIdx];
//         }
//     }
//
//     void evalOutFlowDof_(const int scvIdx)
//     {
//         //calculate outflow fluxes
//         PrimaryVariables values(0.0);
//         const int boundaryFaceIdx = scvIdx;
//         computeFlux(values, boundaryFaceIdx, true);
// //         values *= this->curVolVars_(scvIdx).extrusionFactor();
//         const auto &bcTypes = this->bcTypes_(scvIdx);
//         Valgrind::CheckDefined(values);
//         for (int equationIdx = 0; equationIdx < numEq; ++equationIdx)
//         {
//             if (!bcTypes.isOutflow(equationIdx))
//                 continue;
//             // deduce outflow
//             this->residual_[scvIdx][equationIdx] += values[equationIdx];
//         }
//     }

    /*!
     * \brief Helper function that returns the actual saturation for multi-phase models.
     *        Necessary because the one-phase volVars in dumux stable do not provide this function.
     *
     * \param volVars The volume variables
     * \param phaseIdx The phase index
     */
    template<class T = TypeTag>
    inline typename std::enable_if<(GET_PROP_VALUE(T, NumPhases) > 1), Scalar>::type
    saturation_(const VolumeVariables &volVars, const int phaseIdx) const
    {
        return volVars.saturation(phaseIdx);
    }

    /*!
     * \brief Helper function that returns 1.0 as the saturation for the single-phase model.
     *        Necessary because the one-phase volVars in dumux stable do not provide this function.
     *
     * \param volVars The volume variables
     * \param phaseIdx The phase index
     */
    template<class T = TypeTag>
    inline typename std::enable_if<(GET_PROP_VALUE(T, NumPhases) < 2), Scalar>::type
    saturation_(const VolumeVariables &volVars, const int phaseIdx) const
    {
        return 1.0;
    }

    /*!
     * \brief Correct the volumeTerm term when using the standard box method
     *
     * \param volumeTerm The volumeTerm term
     * \param scvIdx The scvIdx index
     */
    void correctVolumeTerm_(const Problem& problem, PrimaryVariables &volumeTerm, const SubControlVolume &scv, const VolumeVariables& volVars) const
    {
        // The pore network model does not use the elements' geometric information for
        // evaluatiog the fluxes and volumeTerm terms. These values (e.g. pore volume)
        //are specified in the grid explicitly. The following opertations account for this:

        // correct the multiplication of the volumeTerm term with the volume of the SCV
        // done by implicitlocalresidual
        volumeTerm /= scv.volume();
        //multiply the term with the actual pore volume divided by the number of SCVs
        const auto& spatialParams = problem.spatialParams();
        const Scalar poreVolume = spatialParams.initialPoreVolume(scv.dofIndex());
        const int numAdjacentThroats = spatialParams.numAdjacentThroats(scv.dofIndex());
        volumeTerm *= poreVolume / numAdjacentThroats;
    }

};

}

#endif
