// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \copydoc Dumux::PNMVtkOutputModule
 */

#ifndef DUMUX_PNM_VTK_OUTPUT_MODULE_HH
#define DUMUX_PNM_VTK_OUTPUT_MODULE_HH

#include <dune/grid/common/partitionset.hh>
#include <dumux/io/vtkoutputmodule.hh>

namespace Dumux
{

/*!
 * \ingroup PoreNetworkFlow
 * \brief Adds vtk output fields specific to pore-network models.
 */
template<class TypeTag>
class PNMVtkOutputModule : public VtkOutputModule<TypeTag>
{
    using ParentType = VtkOutputModule<TypeTag>;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using FluxVariables = typename GET_PROP_TYPE(TypeTag, FluxVariables);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using FluxVarsCache = typename GET_PROP_TYPE(TypeTag, FluxVariablesCache);

    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    using Element = typename GridView::template Codim<0>::Entity;
    static constexpr int dim = GridView::dimension;


    struct ThroatScalarDataInfo { std::function<Scalar(const Problem&, const Element&, const ElementVolumeVariables&)> get; std::string name; };
    struct ThroatFluxDataInfo { std::function<Scalar(const Problem&, const FluxVariables&, const FluxVarsCache&)> get; std::string name; };
    struct PoreScalarDataInfo { std::function<Scalar(const Problem&, const SubControlVolume&, const VolumeVariables&)> get; std::string name; };

public:

    //! The constructor
    PNMVtkOutputModule(const Problem& problem,
                       const FVGridGeometry& fvGridGeometry,
                       const GridVariables& gridVariables,
                       const SolutionVector& sol,
                       const std::string& name,
                       const std::string& paramGroup = "",
                       bool verbose = true,
                       Dune::VTK::DataMode dm = Dune::VTK::conforming)
    : ParentType(problem, fvGridGeometry, gridVariables, sol, name, paramGroup, verbose, dm)
    , problem_(problem)
    , gridVariables_(gridVariables)
    , sol_(sol)
    {}

    //! Output a scalar variable related to pore throats. This is basically a wrapper for the ParentType's addField method.
    //! \param f A function taking a Problem, Element and ElementVolumeVariables object and returning the desired scalar
    //! \param name The name of the vtk field
    void addThroatVariable(std::function<Scalar(const Problem&, const Element&, const ElementVolumeVariables&)>&& f, const std::string& name)
    {
        throatScalarDataInfo_.push_back(ThroatScalarDataInfo{f, name});
        const auto numElems = problem_.fvGridGeometry().gridView().size(0);
        throatScalarData_.push_back(std::vector<Scalar>(numElems));
        ParentType::addField(throatScalarData_.back(), throatScalarDataInfo_.back().name, ParentType::FieldType::element);
    }

    //! Output a scalar flux variable related to pore throats. This is basically a wrapper for the ParentType's addField method.
    //! \param f A function taking a Problem, FluxVariables and FluxVarsCache object and returning the desired scalar
    //! \param name The name of the vtk field
    void addThroatFluxVariable(std::function<Scalar(const Problem&, const FluxVariables&, const FluxVarsCache&)>&& f, const std::string& name)
    {
        throatFluxDataInfo_.push_back(ThroatFluxDataInfo{f, name});
        const auto numElems = problem_.fvGridGeometry().gridView().size(0);
        throatFluxData_.push_back(std::vector<Scalar>(numElems));
        ParentType::addField(throatFluxData_.back(), throatFluxDataInfo_.back().name, ParentType::FieldType::element);
    }

    //! Output a scalar variable related to pore bodies. This is basically a wrapper for the ParentType's addField method.
    //! \param f A function taking a Problem, SubControlVolume and VolumeVariables object and returning the desired scalar
    //! \param name The name of the vtk field
    void addPoreVariable(std::function<Scalar(const Problem&, const SubControlVolume&, const VolumeVariables&)>&& f, const std::string& name)
    {
        poreScalarDataInfo_.push_back(PoreScalarDataInfo{f, name});
        const auto numPores = problem_.fvGridGeometry().gridView().size(dim);
        poreScalarData_.push_back(std::vector<Scalar>(numPores));
        ParentType::addField(poreScalarData_.back(), poreScalarDataInfo_.back().name, ParentType::FieldType::vertex);
    }

    //! Gather and process all required data and write them to a vtk file.
    void write(double time, Dune::VTK::OutputType type = Dune::VTK::ascii)
    {
        const auto gridView = problem_.fvGridGeometry().gridView();
        const auto numDofs = gridView.size(dim);
        const auto numElems = gridView.size(0);

        // resize all fields
        for(auto& data : throatScalarData_)
            data.resize(numElems);

        for(auto& data : throatFluxData_)
            data.resize(numElems);

        for(auto& data : poreScalarData_)
            data.resize(numDofs);

        // iterate over all elements
        for (const auto& element : elements(gridView, Dune::Partitions::interior))
        {
            // make sure FVElementGeometry & vol vars are bound to the element
            auto fvGeometry = localView(problem_.fvGridGeometry());
            fvGeometry.bindElement(element);

            const auto eIdx = problem_.fvGridGeometry().elementMapper().index(element);

            auto elemVolVars = localView(gridVariables_.curGridVolVars());
            auto elemFluxVarsCache = localView(gridVariables_.gridFluxVarsCache());

            elemVolVars.bind(element, fvGeometry, sol_);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            // treat the throat related data
            std::size_t dataIdx = 0;
            for(auto& data : throatScalarData_)
                data[eIdx] = throatScalarDataInfo_[dataIdx++].get(problem_, element, elemVolVars);

            // treat the throat flux related data
            for(auto&& scvf : scvfs(fvGeometry))
            {
                if(!scvf.boundary())
                {
                    FluxVariables fluxVars;
                    fluxVars.init(problem_, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

                    dataIdx = 0;
                    for(auto& data : throatFluxData_)
                        data[eIdx] = throatFluxDataInfo_[dataIdx++].get(problem_, fluxVars, elemFluxVarsCache[scvf]);
                }

            }

            // treat the pore body related data
            for(const auto& scv : scvs(fvGeometry))
            {
                dataIdx = 0;
                for (auto& data : poreScalarData_)
                    data[scv.dofIndex()] = poreScalarDataInfo_[dataIdx++].get(problem_, scv, elemVolVars[scv]);
            }
        }

        // call the ParentType's write method to write out all data
        ParentType::write(time, type);

        // empty the data containers in order to save some memory
        auto clearAndShrink = [] (auto& data)
        {
            data.clear();
            data.shrink_to_fit();
        };

        for(auto& data : throatScalarData_)
            clearAndShrink(data);
        for(auto& data : throatFluxData_)
            clearAndShrink(data);
        for(auto& data : poreScalarData_)
            clearAndShrink(data);
    }

private:

    const Problem& problem_;
    const GridVariables& gridVariables_;
    const SolutionVector& sol_;

    std::vector<ThroatScalarDataInfo> throatScalarDataInfo_;
    std::list<std::vector<Scalar>> throatScalarData_;

    std::vector<ThroatFluxDataInfo> throatFluxDataInfo_;
    std::list<std::vector<Scalar>> throatFluxData_;

    std::vector<PoreScalarDataInfo> poreScalarDataInfo_;
    std::list<std::vector<Scalar>> poreScalarData_;
};

} //namespace Dumux


#endif // DUMUX_PNM_VTK_OUTPUT_MODULE_HH
