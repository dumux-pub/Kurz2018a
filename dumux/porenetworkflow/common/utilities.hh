// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains functions useful for all types of pore-network models,
 *        e.g. for the calculation of fluxes at the boundary.
 *
 */
#ifndef DUMUX_PNM_UTILITES_HH
#define DUMUX_PNM_UTILITES_HH

#include <cmath>
#include <dumux/common/parameters.hh>

namespace Dumux
{

// TODO: find a better way to check onBoundary
template <class Problem>
bool dofOnBoundary(const Problem& problem, const int dofIndex)
{
    if(problem.spatialParams().useBoundaryDataFromSpatialParams())
        return problem.spatialParams().poreLabel(dofIndex) > 0 /*|| problem.fvGridGeometry().dofOnBoundary(dofIndex)*/;
    else
        return problem.fvGridGeometry().dofOnBoundary(dofIndex);
}

template<class TypeTag>
class Averaging
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);

    static constexpr auto dim = GridView::dimension;
    static constexpr auto dimWorld = GridView::dimensionworld;
    static constexpr auto dofCodim = dim;

public:
     /*!
     * \brief Returns volume averaged quantities like saturation and pressure
     *
     * \param problem The problem
     *
     * Joekar-Niasar et al. (2010) \cite joekar-niasar2010
     *
     */
    template<class T = TypeTag, typename = typename std::enable_if<(GET_PROP_VALUE(T, NumPhases) == 2), void>::type>
    static auto averagedValues(const Problem &problem,
                               const GridVariables& gridVariables,
                               const SolutionVector& sol,
                               const bool neglectInletOutlet = false)
    {
        struct Values
        {
          Scalar sw;
          Scalar sn;
          Scalar pw;
          Scalar pn;
          Scalar pc;
        };
        Values values;

        auto& gridView = problem.fvGridGeometry().gridView();

        Scalar totalPoreVolume = 0;

        Scalar svW = 0;
        Scalar svN = 0;
        Scalar psvW = 0;
        Scalar psvN = 0;

        std::vector<bool> poreVisited(gridView.size(dim));
        std::fill(poreVisited.begin(), poreVisited.end(), false);

        // neglect the pores attached to inlets and outlets
        if(neglectInletOutlet)
        {
            for(auto&& element : elements(gridView))
            {
                if(problem.spatialParams().isInletThroat(element) || problem.spatialParams().isOutletThroat(element))
                {
                    for(int scvIdx = 0; scvIdx < 2; ++scvIdx)
                    {
                        const int dofIdxGlobal = problem.fvGridGeometry().vertexMapper().subIndex(element, scvIdx, dofCodim);
                        poreVisited[dofIdxGlobal] = true;
                    }
                }
            }
        }

        for(auto&& element : elements(gridView))
        {
            if(element.partitionType() == Dune::InteriorEntity)
            {
                auto fvGeometry = localView(problem.fvGridGeometry());
                fvGeometry.bind(element);

                auto elemVolVars = localView(gridVariables.curGridVolVars());
                elemVolVars.bind(element, fvGeometry, sol);


                for (int scvIdx = 0; scvIdx < fvGeometry.numScv(); ++scvIdx)
                {
                    const int dofIdxGlobal =problem.fvGridGeometry().vertexMapper().subIndex(element, scvIdx, dofCodim);

                    if(poreVisited[dofIdxGlobal] )//|| problem.model().onBoundary(dofIdxGlobal))
                        continue;
                    else
                    {
                        const Scalar sw = elemVolVars[scvIdx].saturation(Indices::wPhaseIdx);
                        const Scalar sn = elemVolVars[scvIdx].saturation(Indices::nPhaseIdx);
                        const Scalar pw = elemVolVars[scvIdx].pressure(Indices::wPhaseIdx);
                        const Scalar pn = elemVolVars[scvIdx].pressure(Indices::nPhaseIdx);
                        const Scalar poreVolume = problem.spatialParams().poreVolume(dofIdxGlobal);
                        totalPoreVolume += poreVolume;

                        svW  += sw*poreVolume;
                        psvW += pw*svW;
                        svN  += sn*poreVolume;
                        psvN += pn*svN;

                        poreVisited[dofIdxGlobal] = true;
                    }
                }
            }
        }
        // average saturations
        values.sw = svW / totalPoreVolume;
        values.sn = 1.0 - values.sw;

        // average pressures
        values.pw = psvW / svW;
        values.pn = psvN / svN;
        values.pc = values.pn - values.pw;

        return values;
     }
};


} // end namespace

#endif
