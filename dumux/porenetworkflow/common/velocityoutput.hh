// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Velocity output for implicit (porous media) models
 */
#ifndef DUMUX_PNM_VELOCITYOUTPUT_HH
#define DUMUX_PNM_VELOCITYOUTPUT_HH

#include <dune/common/fvector.hh>
#include <dumux/common/properties.hh>

namespace Dumux
{

namespace Properties
{
    NEW_PROP_TAG(VtkAddVelocity); //!< Returns whether velocity vectors are written into the vtk output
}

/*!
 * \brief Velocity output for implicit (porous media) models
 */
template<class TypeTag>
class PNMVelocityOutput
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using FluxVariables = typename GET_PROP_TYPE(TypeTag, FluxVariables);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);

    // static constexpr bool isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox);
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    static constexpr int numPhases = GET_PROP_VALUE(TypeTag, NumPhases);

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

public:
    /*!
     * \brief Constructor initializes the static data with the initial solution.
     *
     * \param problem The problem to be solved
     */
    PNMVelocityOutput(const Problem& problem,
                      const FVGridGeometry& fvGridGeometry,
                      const GridVariables& gridVariables,
                      const SolutionVector& sol)
    :
    problem_(problem),
    fvGridGeometry_(fvGridGeometry),
    gridVariables_(gridVariables),
    sol_(sol)
    {
        velocityOutput_ = getParamFromGroup<bool>(GET_PROP_VALUE(TypeTag, ModelParameterGroup), "Vtk.AddVelocity");
    }

    bool enableOutput()
    { return velocityOutput_; }

    //! Calculate the velocities for the scvs in the element
    //! We assume the local containers to be bound to the complete stencil
    template<class VelocityVector>
    void calculateVelocity(VelocityVector& velocity,
                           const ElementVolumeVariables& elemVolVars,
                           const FVElementGeometry& fvGeometry,
                           const Element& element,
                           int phaseIdx)
    {
        if (!velocityOutput_) return;

        const auto geometry = element.geometry();

        // bind the element flux variables cache
        auto elemFluxVarsCache = localView(gridVariables_.gridFluxVarsCache());
        elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

        GlobalPosition tmpVelocity(0.0);
        tmpVelocity = (geometry.corner(1) - geometry.corner(0));
        tmpVelocity /= tmpVelocity.two_norm();

        const int eIdxGlobal = fvGridGeometry_.elementMapper().index(element);
        velocity[eIdxGlobal] = 0.0;

        for (auto&& scvf : scvfs(fvGeometry))
        {
            if (scvf.boundary())
                continue;

            // insantiate the flux variables
            FluxVariables fluxVars;
            fluxVars.init(problem_, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

            // get the volume flux divided by the area of the
            // subcontrolvolume face in the reference element
            // TODO: Divide by extrusion factor!!?
            const Scalar flux = getFlux_(element, elemVolVars, fluxVars, phaseIdx, std::integral_constant<int, numPhases>());

            tmpVelocity *= flux;
            velocity[eIdxGlobal] = tmpVelocity;
        }
    }

private:

    //! overload for 1p models
    Scalar getFlux_(const Element& element,
                    const ElementVolumeVariables& elemVolVars,
                    const FluxVariables& fluxVars,
                    const int phaseIdx,
                    std::integral_constant<int, 1>)
    {
        // the upwind term to be used for the volume flux evaluation
        auto upwindTerm = [phaseIdx](const auto& volVars) { return volVars.mobility(phaseIdx); };
        const Scalar localArea = problem_.spatialParams().throatCrossSection(element, elemVolVars);

        return fluxVars.advectiveFlux(phaseIdx, upwindTerm) / localArea;
    }

    //! overload for 2p models
    Scalar getFlux_(const Element& element,
                    const ElementVolumeVariables& elemVolVars,
                    const FluxVariables& fluxVars,
                    const int phaseIdx,
                    std::integral_constant<int, 2>)
    {
        // auto elemFluxVarsCache = localView(gridVariables_.gridFluxVarsCache());
        // elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);
        // gridVariables_.gridFluxVarsCache().invaded(element)


        // the upwind term to be used for the volume flux evaluation
        auto upwindTerm = [phaseIdx](const auto& volVars) { return volVars.mobility(phaseIdx); };
        const Scalar localArea = problem_.spatialParams().throatCrossSection(element, elemVolVars, phaseIdx);
        const bool invaded = elemVolVars.gridVolVars().invaded(element);
        // const bool invaded = problem_.model().isInvaded(element);

        // early return of 0.0 if throat is not invaded (otherwise localArea == 0, which results in NAN results)
        if(!invaded)
            return 0.0;

        return fluxVars.advectiveFlux(phaseIdx, upwindTerm) / localArea;
    }

    const Problem& problem_;
    bool velocityOutput_;

    const FVGridGeometry& fvGridGeometry_;
    const GridVariables& gridVariables_;
    const SolutionVector& sol_;
};

} // end namespace Dumux

#endif
