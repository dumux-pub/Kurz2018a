// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup PoreNetworkFlow
 * \copydoc Dumux::PNMCommonVtkOutputFields
 */
#ifndef DUMUX_PNM_COMMON_VTK_OUTPUT_FIELDS_HH
#define DUMUX_PNM_COMMON_VTK_OUTPUT_FIELDS_HH


namespace Dumux
{

/*!
 * \ingroup PoreNetworkFlow
 * \brief Adds vtk output fields specific to all pore-network models
 */
class PNMCommonVtkOutputFields
{

public:
    template <class VtkOutputModule>
    static void init(VtkOutputModule& vtk)
    {
        vtk.addPoreVariable([](const auto& problem, const auto& scv, const auto& volVars)
                           { return scv.dofIndex(); }, "dofIndex");
        vtk.addPoreVariable([](const auto& problem, const auto& scv, const auto& volVars)
                           { return problem.spatialParams().numAdjacentThroats(scv.dofIndex()); },
                             "coordinationNumber");
        vtk.addPoreVariable([](const auto& problem, const auto& scv, const auto& volVars)
                           { return problem.spatialParams().poreLabel(scv.dofIndex()); },
                             "poreLabel");
        vtk.addPoreVariable([](const auto& problem, const auto& scv, const auto& volVars)
                           { return problem.spatialParams().poreRadius(scv.dofIndex(), volVars); },
                             "poreRadius");

        vtk.addThroatVariable([](const auto& problem, const auto& element, const auto& elemVolVars)
                             { return problem.spatialParams().throatLabel(element); }, "throatLabel");
        vtk.addThroatVariable([](const auto& problem, const auto& element, const auto& elemVolVars)
                             { return problem.spatialParams().throatRadius(element, elemVolVars); }, "throatRadius");
        vtk.addThroatVariable([](const auto& problem, const auto& element, const auto& elemVolVars)
                             { return problem.spatialParams().throatLength(element, elemVolVars); }, "throatLength");
    }
};

} // end namespace Dumux

#endif
