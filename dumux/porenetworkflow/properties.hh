// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup PorenetworkFlow
 * \file
 *
 * \brief Defines common properties required for all pore-network models.
 */
#ifndef DUMUX_PNM_COMMON_PROPERTIES_HH
#define DUMUX_PNM_COMMON_PROPERTIES_HH

#include <dumux/discretization/box/properties.hh>

#include <dumux/porousmediumflow/fluxvariables.hh>
#include <dumux/common/properties/model.hh>


#include <dumux/io/porenetworkgridcreator.hh>
#include <dumux/porenetworkflow/common/fickslaw.hh>
#include <dumux/porenetworkflow/common/fourierslaw.hh>
#include <dumux/porenetworkflow/common/advection.hh>
#include <dumux/porenetworkflow/common/localresidual.hh>
#include <dumux/porenetworkflow/common/labels.hh>
#include <dumux/porenetworkflow/common/velocityoutput.hh>
#include <dumux/porenetworkflow/common/elementboundarytypes.hh>
#include <dumux/porenetworkflow/common/utilities.hh>
#include <dumux/porenetworkflow/common/fluxvariablescache.hh>
#include <dumux/material/fluidmatrixinteractions/porenetwork/transmissibility.hh>

namespace Dumux
{

namespace Properties {

//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tag for the pore-network problem
NEW_TYPE_TAG(PoreNetworkModel, INHERITS_FROM(BoxModel, ModelProperties));

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////

NEW_PROP_TAG(GridBoundaryDefinition); //!< Defines method to evaluate boundaries
NEW_PROP_TAG(PoreGeometry); //!< The pore geometry
NEW_PROP_TAG(ThroatGeometry); //!< The throat geometry
NEW_PROP_TAG(SinglePhaseTransmissibility); //!< The transmissibility type
NEW_PROP_TAG(Labels); //!< The pore/throat labels
NEW_PROP_TAG(NeglectPoreFlowResistance);


SET_PROP(PoreNetworkModel, AdvectionType)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using TransmissibilityType = ThroatTransmissibility<TypeTag>;
public:
    using type = Dumux::WashburnAdvection<Scalar, TransmissibilityType, DiscretizationMethod::box>;
};

//! We use fick's law as the default for the diffusive fluxes
SET_TYPE_PROP(PoreNetworkModel, MolecularDiffusionType, Dumux::PNMFicksLaw<TypeTag>);
SET_TYPE_PROP(PoreNetworkModel, HeatConductionType, Dumux::PNMFouriersLaw<TypeTag>);
//! Isothermal model by default
SET_BOOL_PROP(PoreNetworkModel, EnableEnergyBalance, false);

//! The flux variables cache
SET_PROP(PoreNetworkModel, FluxVariablesCache)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using AdvectionType = typename GET_PROP_TYPE(TypeTag, AdvectionType);
    static constexpr auto numPhases = GET_PROP_VALUE(TypeTag, NumPhases);
public:
    using type = PNMFluxVariablesCache<Scalar, AdvectionType, numPhases>;
};

//! The labels
SET_TYPE_PROP(PoreNetworkModel, Labels, Dumux::Labels);

//! The grid creator
SET_TYPE_PROP(PoreNetworkModel, GridCreator, Dumux::PoreNetworkGridCreator<TypeTag>);
SET_TYPE_PROP(PoreNetworkModel, VelocityOutput, Dumux::PNMVelocityOutput<TypeTag>);

SET_BOOL_PROP(PoreNetworkModel, NeglectPoreFlowResistance, true);

SET_BOOL_PROP(PoreNetworkModel, EnableThermalNonEquilibrium, false);


SET_TYPE_PROP(PoreNetworkModel, ElementBoundaryTypes, PNMElementBoundaryTypes<TypeTag>);
// \}
}

} // end namespace

#endif
