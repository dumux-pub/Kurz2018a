cd ..

# dune-common
# master # 62a0c610f1b1a98871c39a77848917b595b5d670 # 2018-03-20 09:18:47 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout master
git reset --hard 62a0c610f1b1a98871c39a77848917b595b5d670
cd ..

# dune-istl
# master # 863be7608490428d40ba743247ad2528890ac75a # 2018-03-08 08:09:34 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout master
git reset --hard 863be7608490428d40ba743247ad2528890ac75a
cd ..

# dune-geometry
# master # 081c7fb83a5a5e5a5256cca4a6ddc4fc07526b29 # 2018-03-19 04:23:06 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout master
git reset --hard 081c7fb83a5a5e5a5256cca4a6ddc4fc07526b29
cd ..

# dune-grid
# master # 776265fd43ddf73ef46183d1895c57926cc9c454 # 2018-03-23 14:40:34 +0000 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout master
git reset --hard 776265fd43ddf73ef46183d1895c57926cc9c454
cd ..

# dune-foamgrid
# master # 5107b112f11f100744fb0d475bee94c84819b8a2 # 2018-01-02 14:30:30 +0000 # Timo Koch
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout master
git reset --hard 5107b112f11f100744fb0d475bee94c84819b8a2
cd ..

# dune-localfunctions
# master # 7c82255b16ac233c1e881fe1d48d74c14e79e66b # 2018-02-06 15:06:58 +0000 # Martin Nolte
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout master
git reset --hard 7c82255b16ac233c1e881fe1d48d74c14e79e66b
cd ..

# dumux
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout master
git reset --hard d9304b25a8a3b80c9c5a9eaa67f8a58e4501e6a4
git am ../Kurz2018a/dumux_patch.patch
cd ..

### Run dunecontrol
./dune-common/bin/dunecontrol --opts=./dumux/optim.opts all


